/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */

package edu.arizona.space.moc.jnaspice2

import groovy.xml.XmlSlurper
import java.io.File
import java.io.FileOutputStream

def typeMap = [ 
    "void": "void",
    "void *" : "byte[]",
    "const void *" : "byte[]",
    
    "SpiceInt" : "int",
    "SpiceInt *" : "int[]",
    "int": "int",
    
    "SpiceDouble" : "double", 
    "doublereal" : "double", 
    "SpiceDouble *" : "double[]", 
    "doublereal *" : "double[]", 
    "ConstSpiceDouble": "double",
    "ConstSpiceDouble *" : "double[]", 
    
    "ConstSpiceChar *" : "String",
    "SpiceChar *" : "String",
    "SpiceChar ***" : "String[]",
    
    "SpiceBoolean" : "boolean",
    "SpiceBoolean *" : "boolean[]",
    
    "SpiceChar" : "byte",
];

def lines = ["""/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */

package edu.arizona.space.moc.jnaspice2;

import com.sun.jna.Library;

public interface JnaSpiceLibrary extends Library {

"""];

new File("doxygen/xml/").listFiles().sort{it.name}.each({ f->
        if(f.name.contains("__c_8c.xml") && !f.name.startsWith("zz")) {
            def contents = f.text;
            def xml = new XmlSlurper().parseText(contents);
            def functions = xml.'**'.find { memberdef -> memberdef.@kind == "function" };
            functions.each(fxn -> {
                    def c_spec = "${fxn.definition} ${fxn.argsstring}";
                    def j_params = [];
                    fxn.param.each(p -> {
                            arr = p.array.size() == 0 ? "" : "[]";
                            if(!p.type.toString().contentEquals("void")) j_params << "${typeMap[p.type]}${arr} ${p.declname}";
                        });
                    def j_spec = "${typeMap[fxn.type]} ${fxn.name} ( ${j_params.join(", ")} )";
                    if(!j_spec.contains("null")) lines << "\t// ${c_spec};\n\t${j_spec};\n\n";
                }
            );        
        }
    });

lines << "};\n\n"

def outFile = new File("src/main/java/edu/arizona/space/moc/jnaspice2/JnaSpiceLibrary.java")
outFile.createNewFile()
def fos = new FileOutputStream(outFile)

lines.each(line -> fos.write(line.getBytes()));