# JNASPICE.

[NAIF](https://naif.jpl.nasa.gov/naif/), creators of the invaluable SPICE Toolkit, 
have four officially supported programming languages: Fortran (where development 
is current done), C, MATLAB, and IDL. Python bindings are available from others. 
NAIF also provides a Java-compatible version via JNI. This has the disadvantage
of requiring code be compiled with the native library, and that specifications of 
method signatures are also required.

This project, JNASpice, hopes to improve upon that situation in two ways:
* First, use JNA instead of JNI so we don't have to add code to SPICE;
* Second, automatically generate method signatures so that we don't have to do it manually. 
  To automatically generate method signatures, I used Doxygen to generate documentation
  from the CSPICE source; then, I patched it to make the XML that it generates more
  easily used; lastly, there is a groovy script that parses the Doxygen XML and 
  generates the source for a class. This is all contained in the `docs` directory.
  This is not integrated into the build process because the CSPICE source isn't
  mine to redistribute.

The current version of CSPICE is N0067; the current version of JNASPICE is 67.0.

Shared library versions of CSPICE are from [cspice+cmake](https://gitlab.com/arizona-astrolabe/desktop/cspice-cmake).

To use this library, add the following to your Maven `pom.xml`:
```
<repositories>
  <repository>
    <id>astrolab-maven-public</id>
    <url>https://gitlab.com/api/v4/projects/58889899/packages/maven</url>
  </repository>
</repositories>
<dependencies>
  <dependency>
    <groupId>edu.arizona.space</groupId>
    <artifactId>jnaspice</artifactId>
    <version>67.0</version>
  </dependency>
</dependencies>

```

---

Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
of Arizona.

This file is part of Astrolabe.

Astrolabe is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

Astrolabe is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
Astrolabe. If not, see <https://www.gnu.org/licenses/>.

![GNU General Public License v3 logo](gplv3-with-text-136x68.png)
