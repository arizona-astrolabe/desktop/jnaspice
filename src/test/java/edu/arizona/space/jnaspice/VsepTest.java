/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package edu.arizona.space.jnaspice;

import org.apache.commons.math3.random.MersenneTwister;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

/**
 *
 * @author agardner
 */
public class VsepTest {

    private static final int ITERATIONS = 10000;
    private final MersenneTwister mt = new MersenneTwister(1382958545); // See https://oeis.org/A000110.

    /**
     * Compute the angular distance between two vectors in RA and Dec directly.
     *
     * @see https://en.wikipedia.org/wiki/Angular_distance
     * @param ra1 Right ascension of the first vector.
     * @param dec1 Declination of the first vector.
     * @param ra2 Right ascension of the second vector.
     * @param dec2 Declination of the second vector.
     * @return The angular distance between the two vectors.
     */
    public static double vsep_radec(double ra1, double dec1, double ra2, double dec2) {
        return Math.acos(Math.sin(dec1) * Math.sin(dec2) + Math.cos(dec1) * Math.cos(dec2) * Math.cos(ra1 - ra2));
    }

    public VsepTest() {
    }

    /**
     * A test to compute angular distance at arbitrary range with rectangular coordinates.
     */
    @Test
    @DisplayName("vsep_c")
    void test1() {
        var jsl = JnaSpiceLibraryFactory.getInstance();
        double[] x = new double[3], y = new double[3];
        jsl.radrec_c(Math.pow(10, 16), Math.toRadians(0), Math.toRadians(-89), x);
        jsl.radrec_c(Math.pow(10, 16), Math.toRadians(180), Math.toRadians(89), y);
        double z = jsl.vsep_c(x, y), zd = Math.toDegrees(z);
        assertTrue(Math.abs(Math.PI - z) < Math.pow(10, -9));
        assertTrue(Math.abs(180 - zd) < Math.pow(10, -9));
    }

    /**
     * A test to compute angular distance from RA and dec.
     */
    @Test
    @DisplayName("angular distance")
    void test2() {
        double ra1 = 0, dec1 = -Math.PI / 2, ra2 = Math.PI, dec2 = Math.PI / 2;
        double z = vsep_radec(ra1, dec1, ra2, dec2);
        assertTrue(Math.abs(Math.PI - z) < Math.pow(10, -9));
    }

    @Test
    @DisplayName("Compare SPICE and direct computation of angular distance")
    void test3() {
        var jsl = JnaSpiceLibraryFactory.getInstance();
        for (int i = 0; i < ITERATIONS; i++) {
            double ra1 = nextRA(), dec1 = nextDec();
            double ra2 = nextRA(), dec2 = nextDec();

            double z1 = vsep_radec(ra1, dec1, ra2, dec2);

            double[] x2 = new double[3], y2 = new double[3];
            jsl.radrec_c(Math.pow(10, 16), ra1, dec1, x2);
            jsl.radrec_c(Math.pow(10, 16), ra2, dec2, y2);
            double z2 = jsl.vsep_c(x2, y2);

            assertTrue(Math.abs(z1 - z2) < Math.pow(10, -9));
        }
    }

    private double nextRA() {
        return mt.nextDouble() * Math.PI * 2;
    }

    private double nextDec() {
        return mt.nextDouble() * Math.PI - (Math.PI / 2);
    }

}
