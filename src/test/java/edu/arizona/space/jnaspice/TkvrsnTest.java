/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package edu.arizona.space.jnaspice;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

/**
 *
 * @author agardner
 */
public class TkvrsnTest {

    private static final Logger LOG = Logger.getLogger(TkvrsnTest.class.getName());

    public TkvrsnTest() {
    }

    @Test
    @DisplayName("tkvrsn_c")
    void test() {
        var jsl = JnaSpiceLibraryFactory.getInstance();
        String ver = jsl.tkvrsn_c("TOOLKIT");
        assertEquals("CSPICE_N0067", ver);
        LOG.log(Level.INFO, ver);
    }
}
