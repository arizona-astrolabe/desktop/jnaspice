/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package edu.arizona.space.jnaspice;

import com.sun.jna.Library;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An interface that defines the methods that can be called through JNA.
 *
 * @author agardner
 */
public interface JnaSpiceLibrary extends Library {

    // void axisar_c (ConstSpiceDouble axis[3], SpiceDouble angle, SpiceDouble r[3][3]);
    void axisar_c(double[] axis, double angle, double[] r);

    // void azlcpo_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *abcorr, SpiceBoolean azccw, SpiceBoolean elplsz, ConstSpiceDouble obspos[3], ConstSpiceChar *obsctr, ConstSpiceChar *obsref, SpiceDouble azlsta[6], SpiceDouble *lt);
    void azlcpo_c(String method, String target, double et, String abcorr, boolean azccw, boolean elplsz, double[] obspos, String obsctr, String obsref, double[] azlsta, double[] lt);

    // void azlrec_c (SpiceDouble range, SpiceDouble az, SpiceDouble el, SpiceBoolean azccw, SpiceBoolean elplsz, SpiceDouble rectan[3]);
    void azlrec_c(double range, double az, double el, boolean azccw, boolean elplsz, double[] rectan);

    // SpiceDouble b1900_c (void);
    double b1900_c();

    // SpiceDouble b1950_c (void);
    double b1950_c();

    // SpiceBoolean badkpv_c (ConstSpiceChar *caller, ConstSpiceChar *name, ConstSpiceChar *comp, SpiceInt size, SpiceInt divby, SpiceChar type);
    boolean badkpv_c(String caller, String name, String comp, int size, int divby, byte type);

    // void bodc2n_c (SpiceInt code, SpiceInt namlen, SpiceChar *name, SpiceBoolean *found);
    void bodc2n_c(int code, int namlen, String name, boolean[] found);

    // void bodc2s_c (SpiceInt code, SpiceInt namlen, SpiceChar *name);
    void bodc2s_c(int code, int namlen, String name);

    // void boddef_c (ConstSpiceChar *name, SpiceInt code);
    void boddef_c(String name, int code);

    // SpiceBoolean bodfnd_c (SpiceInt body, ConstSpiceChar *item);
    boolean bodfnd_c(int body, String item);

    // void bodn2c_c (ConstSpiceChar *name, SpiceInt *code, SpiceBoolean *found);
    void bodn2c_c(String name, int[] code, boolean[] found);

    // void bods2c_c (ConstSpiceChar *name, SpiceInt *code, SpiceBoolean *found);
    void bods2c_c(String name, int[] code, boolean[] found);

    // void bodvar_c (SpiceInt body, ConstSpiceChar *item, SpiceInt *dim, SpiceDouble *values);
    void bodvar_c(int body, String item, int[] dim, double[] values);

    // void bodvcd_c (SpiceInt bodyid, ConstSpiceChar *item, SpiceInt maxn, SpiceInt *dim, SpiceDouble *values);
    void bodvcd_c(int bodyid, String item, int maxn, int[] dim, double[] values);

    // void bodvrd_c (ConstSpiceChar *bodynm, ConstSpiceChar *item, SpiceInt maxn, SpiceInt *dim, SpiceDouble *values);
    void bodvrd_c(String bodynm, String item, int maxn, int[] dim, double[] values);

    default double[] bodvrd_c_3(String bodynm, String item) {
        var retv = new double[]{-1.0, -1.0, -1.0};
        var dim = new int[]{0};
        bodvrd_c(bodynm, item, 3, dim, retv);
        if (dim[0] != 3) {
            Logger.getLogger(JnaSpiceLibrary.class.getCanonicalName()).log(Level.SEVERE, "bodvrd_c returned {0} values instead of 3.", Integer.toString(dim[0]));
        }
        return retv;
    }

    // SpiceDouble brcktd_c (SpiceDouble number, SpiceDouble end1, SpiceDouble end2);
    double brcktd_c(double number, double end1, double end2);

    // SpiceInt brckti_c (SpiceInt number, SpiceInt end1, SpiceInt end2);
    int brckti_c(int number, int end1, int end2);

    // SpiceInt bsrchc_c (ConstSpiceChar *value, SpiceInt ndim, SpiceInt arrlen, const void *array);
    int bsrchc_c(String value, int ndim, int arrlen, byte[] array);

    // SpiceInt bsrchd_c (SpiceDouble value, SpiceInt ndim, ConstSpiceDouble *array);
    int bsrchd_c(double value, int ndim, double[] array);

    // void ccifrm_c (SpiceInt frclss, SpiceInt clssid, SpiceInt frnlen, SpiceInt *frcode, SpiceChar *frname, SpiceInt *cent, SpiceBoolean *found);
    void ccifrm_c(int frclss, int clssid, int frnlen, int[] frcode, String frname, int[] cent, boolean[] found);

    // void chbder_c (ConstSpiceDouble *cp, SpiceInt degp, SpiceDouble x2s[2], SpiceDouble x, SpiceInt nderiv, SpiceDouble *partdp, SpiceDouble *dpdxs);
    void chbder_c(double[] cp, int degp, double[] x2s, double x, int nderiv, double[] partdp, double[] dpdxs);

    // void chbigr_c (SpiceInt degp, ConstSpiceDouble cp[], ConstSpiceDouble x2s[2], SpiceDouble x, SpiceDouble *p, SpiceDouble *itgrlp);
    void chbigr_c(int degp, double[] cp, double[] x2s, double x, double[] p, double[] itgrlp);

    // void chbint_c (ConstSpiceDouble cp[], SpiceInt degp, ConstSpiceDouble x2s[2], SpiceDouble x, SpiceDouble *p, SpiceDouble *dpdx);
    void chbint_c(double[] cp, int degp, double[] x2s, double x, double[] p, double[] dpdx);

    // void chbval_c (ConstSpiceDouble cp[], SpiceInt degp, ConstSpiceDouble x2s[2], SpiceDouble x, SpiceDouble *p);
    void chbval_c(double[] cp, int degp, double[] x2s, double x, double[] p);

    // void chkin_c (ConstSpiceChar *module);
    void chkin_c(String module);

    // void chkout_c (ConstSpiceChar *module);
    void chkout_c(String module);

    // void cidfrm_c (SpiceInt cent, SpiceInt namlen, SpiceInt *frcode, SpiceChar *frname, SpiceBoolean *found);
    void cidfrm_c(int cent, int namlen, int[] frcode, String frname, boolean[] found);

    // void ckcls_c (SpiceInt handle);
    void ckcls_c(int handle);

    // void ckfrot_c (SpiceInt inst, SpiceDouble et, SpiceDouble rotate[3][3], SpiceInt *ref, SpiceBoolean *found);
    void ckfrot_c(int inst, double et, double[] rotate, int[] ref, boolean[] found);

    // void ckfxfm_c (SpiceInt inst, SpiceDouble et, SpiceDouble xform[6][6], SpiceInt *ref, SpiceBoolean *found);
    void ckfxfm_c(int inst, double et, double[] xform, int[] ref, boolean[] found);

    // void ckgp_c (SpiceInt inst, SpiceDouble sclkdp, SpiceDouble tol, ConstSpiceChar *ref, SpiceDouble cmat[3][3], SpiceDouble *clkout, SpiceBoolean *found);
    void ckgp_c(int inst, double sclkdp, double tol, String ref, double[] cmat, double[] clkout, boolean[] found);

    // void ckgpav_c (SpiceInt inst, SpiceDouble sclkdp, SpiceDouble tol, ConstSpiceChar *ref, SpiceDouble cmat[3][3], SpiceDouble av[3], SpiceDouble *clkout, SpiceBoolean *found);
    void ckgpav_c(int inst, double sclkdp, double tol, String ref, double[] cmat, double[] av, double[] clkout, boolean[] found);

    // void ckgr02_c (SpiceInt handle, ConstSpiceDouble descr[], SpiceInt recno, SpiceDouble record[]);
    void ckgr02_c(int handle, double[] descr, int recno, double[] record);

    // void ckgr03_c (SpiceInt handle, ConstSpiceDouble descr[], SpiceInt recno, SpiceDouble record[]);
    void ckgr03_c(int handle, double[] descr, int recno, double[] record);

    // void cklpf_c (ConstSpiceChar *fname, SpiceInt *handle);
    void cklpf_c(String fname, int[] handle);

    // void ckmeta_c (SpiceInt ckid, ConstSpiceChar *meta, SpiceInt *idcode);
    void ckmeta_c(int ckid, String meta, int[] idcode);

    // void cknr02_c (SpiceInt handle, ConstSpiceDouble descr[], SpiceInt *nrec);
    void cknr02_c(int handle, double[] descr, int[] nrec);

    // void cknr03_c (SpiceInt handle, ConstSpiceDouble descr[], SpiceInt *nrec);
    void cknr03_c(int handle, double[] descr, int[] nrec);

    // void ckopn_c (ConstSpiceChar *fname, ConstSpiceChar *ifname, SpiceInt ncomch, SpiceInt *handle);
    void ckopn_c(String fname, String ifname, int ncomch, int[] handle);

    // void ckupf_c (SpiceInt handle);
    void ckupf_c(int handle);

    // void ckw01_c (SpiceInt handle, SpiceDouble begtim, SpiceDouble endtim, SpiceInt inst, ConstSpiceChar *ref, SpiceBoolean avflag, ConstSpiceChar *segid, SpiceInt nrec, ConstSpiceDouble sclkdp[], ConstSpiceDouble quats[][4], ConstSpiceDouble avvs[][3]);
    void ckw01_c(int handle, double begtim, double endtim, int inst, String ref, boolean avflag, String segid, int nrec, double[] sclkdp, double[] quats, double[] avvs);

    // void ckw02_c (SpiceInt handle, SpiceDouble begtim, SpiceDouble endtim, SpiceInt inst, ConstSpiceChar *ref, ConstSpiceChar *segid, SpiceInt nrec, ConstSpiceDouble start[], ConstSpiceDouble stop[], ConstSpiceDouble quats[][4], ConstSpiceDouble avvs[][3], ConstSpiceDouble rates[]);
    void ckw02_c(int handle, double begtim, double endtim, int inst, String ref, String segid, int nrec, double[] start, double[] stop, double[] quats, double[] avvs, double[] rates);

    // void ckw03_c (SpiceInt handle, SpiceDouble begtim, SpiceDouble endtim, SpiceInt inst, ConstSpiceChar *ref, SpiceBoolean avflag, ConstSpiceChar *segid, SpiceInt nrec, ConstSpiceDouble sclkdp[], ConstSpiceDouble quats[][4], ConstSpiceDouble avvs[][3], SpiceInt nints, ConstSpiceDouble starts[]);
    void ckw03_c(int handle, double begtim, double endtim, int inst, String ref, boolean avflag, String segid, int nrec, double[] sclkdp, double[] quats, double[] avvs, int nints, double[] starts);

    // void clearc_c (SpiceInt ndim, SpiceInt arrlen, void *array);
    void clearc_c(int ndim, int arrlen, byte[] array);

    // void cleard_c (SpiceInt ndim, SpiceDouble *array);
    void cleard_c(int ndim, double[] array);

    // void cleari_c (SpiceInt ndim, SpiceInt array[]);
    void cleari_c(int ndim, int[] array);

    // SpiceDouble clight_c (void);
    double clight_c();

    // void clpool_c (void);
    void clpool_c();

    // void cmprss_c (SpiceChar delim, SpiceInt n, ConstSpiceChar *input, SpiceInt outlen, SpiceChar *output);
    void cmprss_c(byte delim, int n, String input, int outlen, String output);

    // void cnmfrm_c (ConstSpiceChar *cname, SpiceInt frnlen, SpiceInt *frcode, SpiceChar *frname, SpiceBoolean *found);
    void cnmfrm_c(String cname, int frnlen, int[] frcode, String frname, boolean[] found);

    // void conics_c (ConstSpiceDouble elts[8], SpiceDouble et, SpiceDouble state[6]);
    void conics_c(double[] elts, double et, double[] state);

    // void convrt_c (SpiceDouble x, ConstSpiceChar *in, ConstSpiceChar *out, SpiceDouble *y);
    void convrt_c(double x, String in, String out, double[] y);

    // SpiceInt cpos_c (ConstSpiceChar *str, ConstSpiceChar *chars, SpiceInt start);
    int cpos_c(String str, String chars, int start);

    // SpiceInt cposr_c (ConstSpiceChar *str, ConstSpiceChar *chars, SpiceInt start);
    int cposr_c(String str, String chars, int start);

    // void cvpool_c (ConstSpiceChar *agent, SpiceBoolean *update);
    void cvpool_c(String agent, boolean[] update);

    // void cyllat_c (SpiceDouble r, SpiceDouble clon, SpiceDouble z, SpiceDouble *radius, SpiceDouble *lon, SpiceDouble *lat);
    void cyllat_c(double r, double clon, double z, double[] radius, double[] lon, double[] lat);

    // void cylrec_c (SpiceDouble r, SpiceDouble clon, SpiceDouble z, SpiceDouble rectan[3]);
    void cylrec_c(double r, double clon, double z, double[] rectan);

    // void cylsph_c (SpiceDouble r, SpiceDouble clon, SpiceDouble z, SpiceDouble *radius, SpiceDouble *colat, SpiceDouble *slon);
    void cylsph_c(double r, double clon, double z, double[] radius, double[] colat, double[] slon);

    // void dafac_c (SpiceInt handle, SpiceInt n, SpiceInt buflen, const void *buffer);
    void dafac_c(int handle, int n, int buflen, byte[] buffer);

    // void dafbbs_c (SpiceInt handle);
    void dafbbs_c(int handle);

    // void dafbfs_c (SpiceInt handle);
    void dafbfs_c(int handle);

    // void dafcls_c (SpiceInt handle);
    void dafcls_c(int handle);

    // void dafcs_c (SpiceInt handle);
    void dafcs_c(int handle);

    // void dafdc_c (SpiceInt handle);
    void dafdc_c(int handle);

    // void dafec_c (SpiceInt handle, SpiceInt bufsiz, SpiceInt buffln, SpiceInt *n, void *buffer, SpiceBoolean *done);
    void dafec_c(int handle, int bufsiz, int buffln, int[] n, byte[] buffer, boolean[] done);

    // void daffna_c (SpiceBoolean *found);
    void daffna_c(boolean[] found);

    // void daffpa_c (SpiceBoolean *found);
    void daffpa_c(boolean[] found);

    // void dafgda_c (SpiceInt handle, SpiceInt baddr, SpiceInt eaddr, SpiceDouble *data);
    void dafgda_c(int handle, int baddr, int eaddr, double[] data);

    // void dafgh_c (SpiceInt *handle);
    void dafgh_c(int[] handle);

    // void dafgn_c (SpiceInt namlen, SpiceChar *name);
    void dafgn_c(int namlen, String name);

    // void dafgs_c (SpiceDouble sum[]);
    void dafgs_c(double[] sum);

    // void dafgsr_c (SpiceInt handle, SpiceInt recno, SpiceInt begin, SpiceInt end, SpiceDouble *data, SpiceBoolean *found);
    void dafgsr_c(int handle, int recno, int begin, int end, double[] data, boolean[] found);

    // void dafhsf_c (SpiceInt handle, SpiceInt *nd, SpiceInt *ni);
    void dafhsf_c(int handle, int[] nd, int[] ni);

    // void dafopr_c (ConstSpiceChar *fname, SpiceInt *handle);
    void dafopr_c(String fname, int[] handle);

    // void dafopw_c (ConstSpiceChar *fname, SpiceInt *handle);
    void dafopw_c(String fname, int[] handle);

    // void dafrda_c (SpiceInt handle, SpiceInt begin, SpiceInt end, SpiceDouble *data);
    void dafrda_c(int handle, int begin, int end, double[] data);

    // void dafrfr_c (SpiceInt handle, SpiceInt ifnlen, SpiceInt *nd, SpiceInt *ni, SpiceChar *ifname, SpiceInt *fward, SpiceInt *bward, SpiceInt *free);
    void dafrfr_c(int handle, int ifnlen, int[] nd, int[] ni, String ifname, int[] fward, int[] bward, int[] free);

    // void dafrs_c (ConstSpiceDouble *sum);
    void dafrs_c(double[] sum);

    // void dafus_c (ConstSpiceDouble sum[], SpiceInt nd, SpiceInt ni, SpiceDouble dc[], SpiceInt ic[]);
    void dafus_c(double[] sum, int nd, int ni, double[] dc, int[] ic);

    // void dasac_c (SpiceInt handle, SpiceInt n, SpiceInt buflen, const void *buffer);
    void dasac_c(int handle, int n, int buflen, byte[] buffer);

    // void dasadc_c (SpiceInt handle, SpiceInt n, SpiceInt bpos, SpiceInt epos, SpiceInt datlen, const void *data);
    void dasadc_c(int handle, int n, int bpos, int epos, int datlen, byte[] data);

    // void dasadd_c (SpiceInt handle, SpiceInt n, ConstSpiceDouble data[]);
    void dasadd_c(int handle, int n, double[] data);

    // void dascls_c (SpiceInt handle);
    void dascls_c(int handle);

    // void dasdc_c (SpiceInt handle);
    void dasdc_c(int handle);

    // void dasec_c (SpiceInt handle, SpiceInt bufsiz, SpiceInt buffln, SpiceInt *n, void *buffer, SpiceBoolean *done);
    void dasec_c(int handle, int bufsiz, int buffln, int[] n, byte[] buffer, boolean[] done);

    // void dashfn_c (SpiceInt handle, SpiceInt namlen, SpiceChar *fname);
    void dashfn_c(int handle, int namlen, String fname);

    // void dashfs_c (SpiceInt handle, SpiceInt *nresvr, SpiceInt *nresvc, SpiceInt *ncomr, SpiceInt *ncomc, SpiceInt *free, SpiceInt lastla[3], SpiceInt lastrc[3], SpiceInt lastwd[3]);
    void dashfs_c(int handle, int[] nresvr, int[] nresvc, int[] ncomr, int[] ncomc, int[] free, int[] lastla, int[] lastrc, int[] lastwd);

    // void daslla_c (SpiceInt handle, SpiceInt *lastc, SpiceInt *lastd, SpiceInt *lasti);
    void daslla_c(int handle, int[] lastc, int[] lastd, int[] lasti);

    // void dasllc_c (SpiceInt handle);
    void dasllc_c(int handle);

    // void dasonw_c (ConstSpiceChar *fname, ConstSpiceChar *ftype, ConstSpiceChar *ifname, SpiceInt ncomr, SpiceInt *handle);
    void dasonw_c(String fname, String ftype, String ifname, int ncomr, int[] handle);

    // void dasopr_c (ConstSpiceChar *fname, SpiceInt *handle);
    void dasopr_c(String fname, int[] handle);

    // void dasops_c (SpiceInt *handle);
    void dasops_c(int[] handle);

    // void dasopw_c (ConstSpiceChar *fname, SpiceInt *handle);
    void dasopw_c(String fname, int[] handle);

    // void dasrdc_c (SpiceInt handle, SpiceInt first, SpiceInt last, SpiceInt bpos, SpiceInt epos, SpiceInt datlen, void *data);
    void dasrdc_c(int handle, int first, int last, int bpos, int epos, int datlen, byte[] data);

    // void dasrdd_c (SpiceInt handle, SpiceInt first, SpiceInt last, SpiceDouble data[]);
    void dasrdd_c(int handle, int first, int last, double[] data);

    // void dasrdi_c (SpiceInt handle, SpiceInt first, SpiceInt last, SpiceInt data[]);
    void dasrdi_c(int handle, int first, int last, int[] data);

    // void dasrfr_c (SpiceInt handle, SpiceInt idwlen, SpiceInt ifnlen, SpiceChar *idword, SpiceChar *ifname, SpiceInt *nresvr, SpiceInt *nresvc, SpiceInt *ncomr, SpiceInt *ncomc);
    void dasrfr_c(int handle, int idwlen, int ifnlen, String idword, String ifname, int[] nresvr, int[] nresvc, int[] ncomr, int[] ncomc);

    // void dasudc_c (SpiceInt handle, SpiceInt first, SpiceInt last, SpiceInt bpos, SpiceInt epos, SpiceInt datlen, const void *data);
    void dasudc_c(int handle, int first, int last, int bpos, int epos, int datlen, byte[] data);

    // void dasudd_c (SpiceInt handle, SpiceInt first, SpiceInt last, ConstSpiceDouble data[]);
    void dasudd_c(int handle, int first, int last, double[] data);

    // void daswbr_c (SpiceInt handle);
    void daswbr_c(int handle);

    // void dazldr_c (SpiceDouble x, SpiceDouble y, SpiceDouble z, SpiceBoolean azccw, SpiceBoolean elplsz, SpiceDouble jacobi[3][3]);
    void dazldr_c(double x, double y, double z, boolean azccw, boolean elplsz, double[] jacobi);

    // void dcyldr_c (SpiceDouble x, SpiceDouble y, SpiceDouble z, SpiceDouble jacobi[3][3]);
    void dcyldr_c(double x, double y, double z, double[] jacobi);

    // void deltet_c (SpiceDouble epoch, ConstSpiceChar *eptype, SpiceDouble *delta);
    void deltet_c(double epoch, String eptype, double[] delta);

    // SpiceDouble det_c (ConstSpiceDouble m1[3][3]);
    double det_c(double[] m1);

    // void dgeodr_c (SpiceDouble x, SpiceDouble y, SpiceDouble z, SpiceDouble re, SpiceDouble f, SpiceDouble jacobi[3][3]);
    void dgeodr_c(double x, double y, double z, double re, double f, double[] jacobi);

    // void diags2_c (ConstSpiceDouble symmat[2][2], SpiceDouble diag[2][2], SpiceDouble rotate[2][2]);
    void diags2_c(double[] symmat, double[] diag, double[] rotate);

    // void dlabns_c (SpiceInt handle);
    void dlabns_c(int handle);

    // void dlaens_c (SpiceInt handle);
    void dlaens_c(int handle);

    // void dlaopn_c (ConstSpiceChar *fname, ConstSpiceChar *ftype, ConstSpiceChar *ifname, SpiceInt ncomch, SpiceInt *handle);
    void dlaopn_c(String fname, String ftype, String ifname, int ncomch, int[] handle);

    // void dlatdr_c (SpiceDouble x, SpiceDouble y, SpiceDouble z, SpiceDouble jacobi[3][3]);
    void dlatdr_c(double x, double y, double z, double[] jacobi);

    // void dnearp_c (ConstSpiceDouble state[6], SpiceDouble a, SpiceDouble b, SpiceDouble c, SpiceDouble dnear[6], SpiceDouble dalt[2], SpiceBoolean *found);
    void dnearp_c(double[] state, double a, double b, double c, double[] dnear, double[] dalt, boolean[] found);

    // void dp2hx_c (SpiceDouble number, SpiceInt hxslen, SpiceChar hxstr[], SpiceInt *hxssiz);
    void dp2hx_c(double number, int hxslen, byte[] hxstr, int[] hxssiz);

    // void dpgrdr_c (ConstSpiceChar *body, SpiceDouble x, SpiceDouble y, SpiceDouble z, SpiceDouble re, SpiceDouble f, SpiceDouble jacobi[3][3]);
    void dpgrdr_c(String body, double x, double y, double z, double re, double f, double[] jacobi);

    // SpiceDouble dpmax_c ();
    double dpmax_c();

    // SpiceDouble dpmin_c ();
    double dpmin_c();

    // SpiceDouble dpr_c (void);
    double dpr_c();

    // void drdazl_c (SpiceDouble range, SpiceDouble az, SpiceDouble el, SpiceBoolean azccw, SpiceBoolean elplsz, SpiceDouble jacobi[3][3]);
    void drdazl_c(double range, double az, double el, boolean azccw, boolean elplsz, double[] jacobi);

    // void drdcyl_c (SpiceDouble r, SpiceDouble clon, SpiceDouble z, SpiceDouble jacobi[3][3]);
    void drdcyl_c(double r, double clon, double z, double[] jacobi);

    // void drdgeo_c (SpiceDouble lon, SpiceDouble lat, SpiceDouble alt, SpiceDouble re, SpiceDouble f, SpiceDouble jacobi[3][3]);
    void drdgeo_c(double lon, double lat, double alt, double re, double f, double[] jacobi);

    // void drdlat_c (SpiceDouble r, SpiceDouble lon, SpiceDouble lat, SpiceDouble jacobi[3][3]);
    void drdlat_c(double r, double lon, double lat, double[] jacobi);

    // void drdpgr_c (ConstSpiceChar *body, SpiceDouble lon, SpiceDouble lat, SpiceDouble alt, SpiceDouble re, SpiceDouble f, SpiceDouble jacobi[3][3]);
    void drdpgr_c(String body, double lon, double lat, double alt, double re, double f, double[] jacobi);

    // void drdsph_c (SpiceDouble r, SpiceDouble colat, SpiceDouble slon, SpiceDouble jacobi[3][3]);
    void drdsph_c(double r, double colat, double slon, double[] jacobi);

    // void dskcls_c (SpiceInt handle, SpiceBoolean optmiz);
    void dskcls_c(int handle, boolean optmiz);

    // void dskgtl_c (SpiceInt keywrd, SpiceDouble *dpval);
    void dskgtl_c(int keywrd, double[] dpval);

    // void dskopn_c (ConstSpiceChar *fname, ConstSpiceChar *ifname, SpiceInt ncomch, SpiceInt *handle);
    void dskopn_c(String fname, String ifname, int ncomch, int[] handle);

    // void dskstl_c (SpiceInt keywrd, SpiceDouble dpval);
    void dskstl_c(int keywrd, double dpval);

    // void dsphdr_c (SpiceDouble x, SpiceDouble y, SpiceDouble z, SpiceDouble jacobi[3][3]);
    void dsphdr_c(double x, double y, double z, double[] jacobi);

    // void dtpool_c (ConstSpiceChar *name, SpiceBoolean *found, SpiceInt *n, SpiceChar type[1]);
    void dtpool_c(String name, boolean[] found, int[] n, byte[] type);

    // void ducrss_c (ConstSpiceDouble s1[6], ConstSpiceDouble s2[6], SpiceDouble sout[6]);
    void ducrss_c(double[] s1, double[] s2, double[] sout);

    // void dvcrss_c (ConstSpiceDouble s1[6], ConstSpiceDouble s2[6], SpiceDouble sout[6]);
    void dvcrss_c(double[] s1, double[] s2, double[] sout);

    // SpiceDouble dvdot_c (ConstSpiceDouble s1[6], ConstSpiceDouble s2[6]);
    double dvdot_c(double[] s1, double[] s2);

    // void dvhat_c (ConstSpiceDouble s1[6], SpiceDouble sout[6]);
    void dvhat_c(double[] s1, double[] sout);

    // SpiceDouble dvnorm_c (ConstSpiceDouble state[6]);
    double dvnorm_c(double[] state);

    // void dvpool_c (ConstSpiceChar *name);
    void dvpool_c(String name);

    // SpiceDouble dvsep_c (ConstSpiceDouble s1[6], ConstSpiceDouble s2[6]);
    double dvsep_c(double[] s1, double[] s2);

    // void ednmpt_c (SpiceDouble a, SpiceDouble b, SpiceDouble c, ConstSpiceDouble normal[3], SpiceDouble point[3]);
    void ednmpt_c(double a, double b, double c, double[] normal, double[] point);

    // void edpnt_c (ConstSpiceDouble p[3], SpiceDouble a, SpiceDouble b, SpiceDouble c, SpiceDouble ep[3]);
    void edpnt_c(double[] p, double a, double b, double c, double[] ep);

    // void edterm_c (ConstSpiceChar *trmtyp, ConstSpiceChar *source, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceInt npts, SpiceDouble *trgepc, SpiceDouble obspos[3], SpiceDouble trmpts[][3]);
    void edterm_c(String trmtyp, String source, String target, double et, String fixref, String abcorr, String obsrvr, int npts, double[] trgepc, double[] obspos, double[] trmpts);

    // void ekappr_c (SpiceInt handle, SpiceInt segno, SpiceInt *recno);
    void ekappr_c(int handle, int segno, int[] recno);

    // void ekccnt_c (ConstSpiceChar *table, SpiceInt *ccount);
    void ekccnt_c(String table, int[] ccount);

    // void ekcls_c (SpiceInt handle);
    void ekcls_c(int handle);

    // void ekdelr_c (SpiceInt handle, SpiceInt segno, SpiceInt recno);
    void ekdelr_c(int handle, int segno, int recno);

    // void ekffld_c (SpiceInt handle, SpiceInt segno, SpiceInt *rcptrs);
    void ekffld_c(int handle, int segno, int[] rcptrs);

    // void ekfind_c (ConstSpiceChar *query, SpiceInt errmln, SpiceInt *nmrows, SpiceBoolean *error, SpiceChar *errmsg);
    void ekfind_c(String query, int errmln, int[] nmrows, boolean[] error, String errmsg);

    // void ekinsr_c (SpiceInt handle, SpiceInt segno, SpiceInt recno);
    void ekinsr_c(int handle, int segno, int recno);

    // void eklef_c (ConstSpiceChar *fname, SpiceInt *handle);
    void eklef_c(String fname, int[] handle);

    // SpiceInt eknelt_c (SpiceInt selidx, SpiceInt row);
    int eknelt_c(int selidx, int row);

    // SpiceInt eknseg_c (SpiceInt handle);
    int eknseg_c(int handle);

    // void ekntab_c (SpiceInt *n);
    void ekntab_c(int[] n);

    // void ekopn_c (ConstSpiceChar *fname, ConstSpiceChar *ifname, SpiceInt ncomch, SpiceInt *handle);
    void ekopn_c(String fname, String ifname, int ncomch, int[] handle);

    // void ekopr_c (ConstSpiceChar *fname, SpiceInt *handle);
    void ekopr_c(String fname, int[] handle);

    // void ekops_c (SpiceInt *handle);
    void ekops_c(int[] handle);

    // void ekopw_c (ConstSpiceChar *fname, SpiceInt *handle);
    void ekopw_c(String fname, int[] handle);

    // void ektnam_c (SpiceInt n, SpiceInt tablen, SpiceChar *table);
    void ektnam_c(int n, int tablen, String table);

    // void ekuef_c (SpiceInt handle);
    void ekuef_c(int handle);

    // void eqncpv_c (SpiceDouble et, SpiceDouble epoch, ConstSpiceDouble eqel[9], SpiceDouble rapol, SpiceDouble decpol, SpiceDouble state[6]);
    void eqncpv_c(double et, double epoch, double[] eqel, double rapol, double decpol, double[] state);

    // SpiceBoolean eqstr_c (ConstSpiceChar *a, ConstSpiceChar *b);
    boolean eqstr_c(String a, String b);

    // void erract_c (ConstSpiceChar *op, SpiceInt actlen, SpiceChar *action);
    void erract_c(String op, int actlen, String action);

    // void errch_c (ConstSpiceChar *marker, ConstSpiceChar *string);
    void errch_c(String marker, String string);

    // void errdev_c (ConstSpiceChar *op, SpiceInt devlen, SpiceChar *device);
    void errdev_c(String op, int devlen, String device);

    // void errdp_c (ConstSpiceChar *marker, SpiceDouble dpnum);
    void errdp_c(String marker, double dpnum);

    // void errint_c (ConstSpiceChar *marker, SpiceInt intnum);
    void errint_c(String marker, int intnum);

    // void errprt_c (ConstSpiceChar *op, SpiceInt lislen, SpiceChar *list);
    void errprt_c(String op, int lislen, String list);

    // void et2lst_c (SpiceDouble et, SpiceInt body, SpiceDouble lon, ConstSpiceChar *type, SpiceInt timlen, SpiceInt ampmlen, SpiceInt *hr, SpiceInt *mn, SpiceInt *sc, SpiceChar *time, SpiceChar *ampm);
    void et2lst_c(double et, int body, double lon, String type, int timlen, int ampmlen, int[] hr, int[] mn, int[] sc, String time, String ampm);

    // void et2utc_c (SpiceDouble et, ConstSpiceChar *format, SpiceInt prec, SpiceInt utclen, SpiceChar *utcstr);
    void et2utc_c(double et, String format, int prec, int utclen, String utcstr);

    // void etcal_c (SpiceDouble et, SpiceInt callen, SpiceChar *calstr);
    void etcal_c(double et, int callen, String calstr);

    // void eul2m_c (SpiceDouble angle3, SpiceDouble angle2, SpiceDouble angle1, SpiceInt axis3, SpiceInt axis2, SpiceInt axis1, SpiceDouble r[3][3]);
    void eul2m_c(double angle3, double angle2, double angle1, int axis3, int axis2, int axis1, double[] r);

    // void eul2xf_c (ConstSpiceDouble eulang[6], SpiceInt axisa, SpiceInt axisb, SpiceInt axisc, SpiceDouble xform[6][6]);
    void eul2xf_c(double[] eulang, int axisa, int axisb, int axisc, double[] xform);

    // void evsgp4_c (SpiceDouble et, ConstSpiceDouble geophs[8], ConstSpiceDouble elems[10], SpiceDouble state[6]);
    void evsgp4_c(double et, double[] geophs, double[] elems, double[] state);

    // SpiceBoolean exists_c (ConstSpiceChar *fname);
    boolean exists_c(String fname);

    // void expool_c (ConstSpiceChar *name, SpiceBoolean *found);
    void expool_c(String name, boolean[] found);

    // SpiceBoolean failed_c ();
    boolean failed_c();

    // void filld_c (SpiceDouble value, SpiceInt ndim, SpiceDouble array[]);
    void filld_c(double value, int ndim, double[] array);

    // void filli_c (SpiceInt value, SpiceInt ndim, SpiceInt array[]);
    void filli_c(int value, int ndim, int[] array);

    // void fovray_c (ConstSpiceChar *inst, ConstSpiceDouble raydir[3], ConstSpiceChar *rframe, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble *et, SpiceBoolean *visibl);
    void fovray_c(String inst, double[] raydir, String rframe, String abcorr, String obsrvr, double[] et, boolean[] visibl);

    // void fovtrg_c (ConstSpiceChar *inst, ConstSpiceChar *target, ConstSpiceChar *tshape, ConstSpiceChar *tframe, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble *et, SpiceBoolean *visibl);
    void fovtrg_c(String inst, String target, String tshape, String tframe, String abcorr, String obsrvr, double[] et, boolean[] visibl);

    // void frame_c (SpiceDouble x[3], SpiceDouble y[3], SpiceDouble z[3]);
    void frame_c(double[] x, double[] y, double[] z);

    // void frinfo_c (SpiceInt frcode, SpiceInt *cent, SpiceInt *frclss, SpiceInt *clssid, SpiceBoolean *found);
    void frinfo_c(int frcode, int[] cent, int[] frclss, int[] clssid, boolean[] found);

    // void frmnam_c (SpiceInt frcode, SpiceInt frnlen, SpiceChar *frname);
    void frmnam_c(int frcode, int frnlen, String frname);

    // void ftncls_c (SpiceInt unit);
    void ftncls_c(int unit);

    // void furnsh_c (ConstSpiceChar *file);
    void furnsh_c(String file);

    // void gcpool_c (ConstSpiceChar *name, SpiceInt start, SpiceInt room, SpiceInt cvalen, SpiceInt *n, void *cvals, SpiceBoolean *found);
    void gcpool_c(String name, int start, int room, int cvalen, int[] n, byte[] cvals, boolean[] found);

    // void gdpool_c (ConstSpiceChar *name, SpiceInt start, SpiceInt room, SpiceInt *n, SpiceDouble *values, SpiceBoolean *found);
    void gdpool_c(String name, int start, int room, int[] n, double[] values, boolean[] found);

    // void georec_c (SpiceDouble lon, SpiceDouble lat, SpiceDouble alt, SpiceDouble re, SpiceDouble f, SpiceDouble rectan[3]);
    void georec_c(double lon, double lat, double alt, double re, double f, double[] rectan);

    // void getcml_c (SpiceInt *argc, SpiceChar ***argv);
    void getcml_c(int[] argc, String[] argv);

    // void getfat_c (ConstSpiceChar *file, SpiceInt arclen, SpiceInt kertln, SpiceChar *arch, SpiceChar *kertyp);
    void getfat_c(String file, int arclen, int kertln, String arch, String kertyp);

    // void getfov_c (SpiceInt instid, SpiceInt room, SpiceInt shapelen, SpiceInt framelen, SpiceChar *shape, SpiceChar *frame, SpiceDouble bsight[3], SpiceInt *n, SpiceDouble bounds[][3]);
    void getfov_c(int instid, int room, int shapelen, int framelen, String shape, String frame, double[] bsight, int[] n, double[] bounds);

    // void getfvn_c (ConstSpiceChar *inst, SpiceInt room, SpiceInt shalen, SpiceInt fralen, SpiceChar *shape, SpiceChar *frame, SpiceDouble bsight[3], SpiceInt *n, SpiceDouble bounds[][3]);
    void getfvn_c(String inst, int room, int shalen, int fralen, String shape, String frame, double[] bsight, int[] n, double[] bounds);

    // void getmsg_c (ConstSpiceChar *option, SpiceInt msglen, SpiceChar *msg);
    void getmsg_c(String option, int msglen, String msg);

    // SpiceBoolean gfbail_c ();
    boolean gfbail_c();

    // void gfclrh_c (void);
    void gfclrh_c();

    // void gfinth_c (int sigcode);
    void gfinth_c(int sigcode);

    // void gfrefn_c (SpiceDouble t1, SpiceDouble t2, SpiceBoolean s1, SpiceBoolean s2, SpiceDouble *t);
    void gfrefn_c(double t1, double t2, boolean s1, boolean s2, double[] t);

    // void gfrepf_c (void);
    void gfrepf_c();

    // void gfrepu_c (SpiceDouble ivbeg, SpiceDouble ivend, SpiceDouble time);
    void gfrepu_c(double ivbeg, double ivend, double time);

    // void gfsstp_c (SpiceDouble step);
    void gfsstp_c(double step);

    // void gfstep_c (SpiceDouble time, SpiceDouble *step);
    void gfstep_c(double time, double[] step);

    // void gfstol_c (SpiceDouble value);
    void gfstol_c(double value);

    // void gipool_c (ConstSpiceChar *name, SpiceInt start, SpiceInt room, SpiceInt *n, SpiceInt *ivals, SpiceBoolean *found);
    void gipool_c(String name, int start, int room, int[] n, int[] ivals, boolean[] found);

    // void gnpool_c (ConstSpiceChar *name, SpiceInt start, SpiceInt room, SpiceInt cvalen, SpiceInt *n, void *cvals, SpiceBoolean *found);
    void gnpool_c(String name, int start, int room, int cvalen, int[] n, byte[] cvals, boolean[] found);

    // SpiceDouble halfpi_c (void);
    double halfpi_c();

    // void hrmesp_c (SpiceInt n, SpiceDouble first, SpiceDouble step, ConstSpiceDouble yvals[], SpiceDouble x, SpiceDouble *f, SpiceDouble *df);
    void hrmesp_c(int n, double first, double step, double[] yvals, double x, double[] f, double[] df);

    // void hrmint_c (SpiceInt n, ConstSpiceDouble *xvals, ConstSpiceDouble *yvals, SpiceDouble x, SpiceDouble *work, SpiceDouble *f, SpiceDouble *df);
    void hrmint_c(int n, double[] xvals, double[] yvals, double x, double[] work, double[] f, double[] df);

    // void hx2dp_c (ConstSpiceChar *string, SpiceInt errmln, SpiceDouble *number, SpiceBoolean *error, SpiceChar errmsg[]);
    void hx2dp_c(String string, int errmln, double[] number, boolean[] error, byte[] errmsg);

    // void ident_c (SpiceDouble matrix[3][3]);
    void ident_c(double[] matrix);

    // void illum_c (ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, ConstSpiceDouble spoint[3], SpiceDouble *phase, SpiceDouble *solar, SpiceDouble *emissn);
    void illum_c(String target, double et, String abcorr, String obsrvr, double[] spoint, double[] phase, double[] solar, double[] emissn);

    // void illumf_c (ConstSpiceChar *method, ConstSpiceChar *target, ConstSpiceChar *ilusrc, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, ConstSpiceDouble spoint[3], SpiceDouble *trgepc, SpiceDouble srfvec[3], SpiceDouble *phase, SpiceDouble *incdnc, SpiceDouble *emissn, SpiceBoolean *visibl, SpiceBoolean *lit);
    void illumf_c(String method, String target, String ilusrc, double et, String fixref, String abcorr, String obsrvr, double[] spoint, double[] trgepc, double[] srfvec, double[] phase, double[] incdnc, double[] emissn, boolean[] visibl, boolean[] lit);

    // void illumg_c (ConstSpiceChar *method, ConstSpiceChar *target, ConstSpiceChar *ilusrc, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, ConstSpiceDouble spoint[3], SpiceDouble *trgepc, SpiceDouble srfvec[3], SpiceDouble *phase, SpiceDouble *incdnc, SpiceDouble *emissn);
    void illumg_c(String method, String target, String ilusrc, double et, String fixref, String abcorr, String obsrvr, double[] spoint, double[] trgepc, double[] srfvec, double[] phase, double[] incdnc, double[] emissn);

    // void ilumin_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, ConstSpiceDouble spoint[3], SpiceDouble *trgepc, SpiceDouble srfvec[3], SpiceDouble *phase, SpiceDouble *incdnc, SpiceDouble *emissn);
    void ilumin_c(String method, String target, double et, String fixref, String abcorr, String obsrvr, double[] spoint, double[] trgepc, double[] srfvec, double[] phase, double[] incdnc, double[] emissn);

    // SpiceInt intmax_c ();
    int intmax_c();

    // SpiceInt intmin_c ();
    int intmin_c();

    // void invert_c (ConstSpiceDouble m[3][3], SpiceDouble mout[3][3]);
    void invert_c(double[] m, double[] mout);

    // void invort_c (ConstSpiceDouble m[3][3], SpiceDouble mit[3][3]);
    void invort_c(double[] m, double[] mit);

    // void invstm_c (ConstSpiceDouble mat[6][6], SpiceDouble invmat[6][6]);
    void invstm_c(double[] mat, double[] invmat);

    // SpiceInt isrchd_c (SpiceDouble value, SpiceInt ndim, ConstSpiceDouble *array);
    int isrchd_c(double value, int ndim, double[] array);

    // SpiceBoolean isrot_c (ConstSpiceDouble m[3][3], SpiceDouble ntol, SpiceDouble dtol);
    boolean isrot_c(double[] m, double ntol, double dtol);

    // SpiceBoolean iswhsp_c (ConstSpiceChar *string);
    boolean iswhsp_c(String string);

    // SpiceDouble j1900_c (void);
    double j1900_c();

    // SpiceDouble j1950_c (void);
    double j1950_c();

    // SpiceDouble j2000_c (void);
    double j2000_c();

    // SpiceDouble j2100_c (void);
    double j2100_c();

    // SpiceDouble jyear_c (void);
    double jyear_c();

    // void kclear_c (void);
    void kclear_c();

    // void kdata_c (SpiceInt which, ConstSpiceChar *kind, SpiceInt fileln, SpiceInt filtln, SpiceInt srclen, SpiceChar *file, SpiceChar *filtyp, SpiceChar *srcfil, SpiceInt *handle, SpiceBoolean *found);
    void kdata_c(int which, String kind, int fileln, int filtln, int srclen, String file, String filtyp, String srcfil, int[] handle, boolean[] found);

    // void kinfo_c (ConstSpiceChar *file, SpiceInt filtln, SpiceInt srclen, SpiceChar *filtyp, SpiceChar *srcfil, SpiceInt *handle, SpiceBoolean *found);
    void kinfo_c(String file, int filtln, int srclen, String filtyp, String srcfil, int[] handle, boolean[] found);

    // void ktotal_c (ConstSpiceChar *kind, SpiceInt *count);
    void ktotal_c(String kind, int[] count);

    // SpiceInt lastnb_c (ConstSpiceChar *string);
    int lastnb_c(String string);

    // void latcyl_c (SpiceDouble radius, SpiceDouble lon, SpiceDouble lat, SpiceDouble *r, SpiceDouble *clon, SpiceDouble *z);
    void latcyl_c(double radius, double lon, double lat, double[] r, double[] clon, double[] z);

    // void latrec_c (SpiceDouble radius, SpiceDouble lon, SpiceDouble lat, SpiceDouble rectan[3]);
    void latrec_c(double radius, double lon, double lat, double[] rectan);

    // void latsph_c (SpiceDouble radius, SpiceDouble lon, SpiceDouble lat, SpiceDouble *rho, SpiceDouble *colat, SpiceDouble *slon);
    void latsph_c(double radius, double lon, double lat, double[] rho, double[] colat, double[] slon);

    // void latsrf_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, SpiceInt npts, ConstSpiceDouble lonlat[][2], SpiceDouble srfpts[][3]);
    void latsrf_c(String method, String target, double et, String fixref, int npts, double[] lonlat, double[] srfpts);

    // void lcase_c (SpiceChar *in, SpiceInt outlen, SpiceChar *out);
    void lcase_c(String in, int outlen, String out);

    // void ldpool_c (ConstSpiceChar *fname);
    void ldpool_c(String fname);

    // SpiceDouble lgresp_c (SpiceInt n, SpiceDouble first, SpiceDouble step, ConstSpiceDouble yvals[], SpiceDouble x);
    double lgresp_c(int n, double first, double step, double[] yvals, double x);

    // void lgrind_c (SpiceInt n, ConstSpiceDouble *xvals, ConstSpiceDouble *yvals, SpiceDouble *work, SpiceDouble x, SpiceDouble *p, SpiceDouble *dp);
    void lgrind_c(int n, double[] xvals, double[] yvals, double[] work, double x, double[] p, double[] dp);

    // SpiceDouble lgrint_c (SpiceInt n, ConstSpiceDouble xvals[], ConstSpiceDouble yvals[], SpiceDouble x);
    double lgrint_c(int n, double[] xvals, double[] yvals, double x);

    // void limbpt_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *corloc, ConstSpiceChar *obsrvr, ConstSpiceDouble refvec[3], SpiceDouble rolstp, SpiceInt ncuts, SpiceDouble schstp, SpiceDouble soltol, SpiceInt maxn, SpiceInt npts[], SpiceDouble points[][3], SpiceDouble epochs[], SpiceDouble tangts[][3]);
    void limbpt_c(String method, String target, double et, String fixref, String abcorr, String corloc, String obsrvr, double[] refvec, double rolstp, int ncuts, double schstp, double soltol, int maxn, int[] npts, double[] points, double[] epochs, double[] tangts);

    // void lparse_c (ConstSpiceChar *list, ConstSpiceChar *delim, SpiceInt nmax, SpiceInt itemln, SpiceInt *n, void *items);
    void lparse_c(String list, String delim, int nmax, int itemln, int[] n, byte[] items);

    // void lparsm_c (ConstSpiceChar *list, ConstSpiceChar *delims, SpiceInt nmax, SpiceInt itemln, SpiceInt *n, void *items);
    void lparsm_c(String list, String delims, int nmax, int itemln, int[] n, byte[] items);

    // SpiceDouble lspcn_c (ConstSpiceChar *body, SpiceDouble et, ConstSpiceChar *abcorr);
    double lspcn_c(String body, double et, String abcorr);

    // SpiceInt lstled_c (SpiceDouble x, SpiceInt n, ConstSpiceDouble *array);
    int lstled_c(double x, int n, double[] array);

    // SpiceInt lstltd_c (SpiceDouble x, SpiceInt n, ConstSpiceDouble *array);
    int lstltd_c(double x, int n, double[] array);

    // void ltime_c (SpiceDouble etobs, SpiceInt obs, ConstSpiceChar *dir, SpiceInt targ, SpiceDouble *ettarg, SpiceDouble *elapsd);
    void ltime_c(double etobs, int obs, String dir, int targ, double[] ettarg, double[] elapsd);

    // void lx4dec_c (ConstSpiceChar *string, SpiceInt first, SpiceInt *last, SpiceInt *nchar);
    void lx4dec_c(String string, int first, int[] last, int[] nchar);

    // void lx4num_c (ConstSpiceChar *string, SpiceInt first, SpiceInt *last, SpiceInt *nchar);
    void lx4num_c(String string, int first, int[] last, int[] nchar);

    // void lx4sgn_c (ConstSpiceChar *string, SpiceInt first, SpiceInt *last, SpiceInt *nchar);
    void lx4sgn_c(String string, int first, int[] last, int[] nchar);

    // void lx4uns_c (ConstSpiceChar *string, SpiceInt first, SpiceInt *last, SpiceInt *nchar);
    void lx4uns_c(String string, int first, int[] last, int[] nchar);

    // void lxqstr_c (ConstSpiceChar *string, SpiceChar qchar, SpiceInt first, SpiceInt *last, SpiceInt *nchar);
    void lxqstr_c(String string, byte qchar, int first, int[] last, int[] nchar);

    // void m2eul_c (ConstSpiceDouble r[3][3], SpiceInt axis3, SpiceInt axis2, SpiceInt axis1, SpiceDouble *angle3, SpiceDouble *angle2, SpiceDouble *angle1);
    void m2eul_c(double[] r, int axis3, int axis2, int axis1, double[] angle3, double[] angle2, double[] angle1);

    // void m2q_c (ConstSpiceDouble r[3][3], SpiceDouble q[4]);
    void m2q_c(double[] r, double[] q);

    // SpiceBoolean matchi_c (ConstSpiceChar *string, ConstSpiceChar *templ, SpiceChar wstr, SpiceChar wchr);
    boolean matchi_c(String string, String templ, byte wstr, byte wchr);

    // SpiceBoolean matchw_c (ConstSpiceChar *string, ConstSpiceChar *templ, SpiceChar wstr, SpiceChar wchr);
    boolean matchw_c(String string, String templ, byte wstr, byte wchr);

    // void mequ_c (ConstSpiceDouble m1[3][3], SpiceDouble mout[3][3]);
    void mequ_c(double[] m1, double[] mout);

    // void moved_c (ConstSpiceDouble arrfrm[], SpiceInt ndim, SpiceDouble arrto[]);
    void moved_c(double[] arrfrm, int ndim, double[] arrto);

    // void mtxm_c (ConstSpiceDouble m1[3][3], ConstSpiceDouble m2[3][3], SpiceDouble mout[3][3]);
    void mtxm_c(double[] m1, double[] m2, double[] mout);

    // void mtxv_c (ConstSpiceDouble m[3][3], ConstSpiceDouble vin[3], SpiceDouble vout[3]);
    void mtxv_c(double[] m, double[] vin, double[] vout);

    // void mxm_c (ConstSpiceDouble m1[3][3], ConstSpiceDouble m2[3][3], SpiceDouble mout[3][3]);
    void mxm_c(double[] m1, double[] m2, double[] mout);

    // void mxmt_c (ConstSpiceDouble m1[3][3], ConstSpiceDouble m2[3][3], SpiceDouble mout[3][3]);
    void mxmt_c(double[] m1, double[] m2, double[] mout);

    // void mxv_c (ConstSpiceDouble m[3][3], ConstSpiceDouble vin[3], SpiceDouble vout[3]);
    void mxv_c(double[] m, double[] vin, double[] vout);

    // void namfrm_c (ConstSpiceChar *frname, SpiceInt *frcode);
    void namfrm_c(String frname, int[] frcode);

    // SpiceInt ncpos_c (ConstSpiceChar *str, ConstSpiceChar *chars, SpiceInt start);
    int ncpos_c(String str, String chars, int start);

    // SpiceInt ncposr_c (ConstSpiceChar *str, ConstSpiceChar *chars, SpiceInt start);
    int ncposr_c(String str, String chars, int start);

    // void nearpt_c (ConstSpiceDouble positn[3], SpiceDouble a, SpiceDouble b, SpiceDouble c, SpiceDouble npoint[3], SpiceDouble *alt);
    void nearpt_c(double[] positn, double a, double b, double c, double[] npoint, double[] alt);

    // void nextwd_c (ConstSpiceChar *string, SpiceInt nexlen, SpiceInt reslen, SpiceChar *next, SpiceChar *rest);
    void nextwd_c(String string, int nexlen, int reslen, String next, String rest);

    // void npedln_c (SpiceDouble a, SpiceDouble b, SpiceDouble c, ConstSpiceDouble linept[3], ConstSpiceDouble linedr[3], SpiceDouble pnear[3], SpiceDouble *dist);
    void npedln_c(double a, double b, double c, double[] linept, double[] linedr, double[] pnear, double[] dist);

    // void nplnpt_c (ConstSpiceDouble linpt[3], ConstSpiceDouble lindir[3], ConstSpiceDouble point[3], SpiceDouble pnear[3], SpiceDouble *dist);
    void nplnpt_c(double[] linpt, double[] lindir, double[] point, double[] pnear, double[] dist);

    // void nthwd_c (ConstSpiceChar *string, SpiceInt nth, SpiceInt worlen, SpiceChar *word, SpiceInt *loc);
    void nthwd_c(String string, int nth, int worlen, String word, int[] loc);

    // void occult_c (ConstSpiceChar *targ1, ConstSpiceChar *shape1, ConstSpiceChar *frame1, ConstSpiceChar *targ2, ConstSpiceChar *shape2, ConstSpiceChar *frame2, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble et, SpiceInt *ocltid);
    void occult_c(String targ1, String shape1, String frame1, String targ2, String shape2, String frame2, String abcorr, String obsrvr, double et, int[] ocltid);

    // void orderd_c (ConstSpiceDouble *array, SpiceInt ndim, SpiceInt *iorder);
    void orderd_c(double[] array, int ndim, int[] iorder);

    // void oscelt_c (ConstSpiceDouble state[6], SpiceDouble et, SpiceDouble mu, SpiceDouble elts[8]);
    void oscelt_c(double[] state, double et, double mu, double[] elts);

    // void oscltx_c (ConstSpiceDouble state[6], SpiceDouble et, SpiceDouble mu, SpiceDouble elts[SPICE_OSCLTX_NELTS]);
    void oscltx_c(double[] state, double et, double mu, double[] elts);

    // void pckcls_c (SpiceInt handle);
    void pckcls_c(int handle);

    // void pcklof_c (ConstSpiceChar *fname, SpiceInt *handle);
    void pcklof_c(String fname, int[] handle);

    // void pckopn_c (ConstSpiceChar *name, ConstSpiceChar *ifname, SpiceInt ncomch, SpiceInt *handle);
    void pckopn_c(String name, String ifname, int ncomch, int[] handle);

    // void pckuof_c (SpiceInt handle);
    void pckuof_c(int handle);

    // void pckw02_c (SpiceInt handle, SpiceInt clssid, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceDouble intlen, SpiceInt n, SpiceInt polydg, SpiceDouble cdata[], SpiceDouble btime);
    void pckw02_c(int handle, int clssid, String frame, double first, double last, String segid, double intlen, int n, int polydg, double[] cdata, double btime);

    // void pdpool_c (ConstSpiceChar *name, SpiceInt n, ConstSpiceDouble *values);
    void pdpool_c(String name, int n, double[] values);

    // void pgrrec_c (ConstSpiceChar *body, SpiceDouble lon, SpiceDouble lat, SpiceDouble alt, SpiceDouble re, SpiceDouble f, SpiceDouble rectan[3]);
    void pgrrec_c(String body, double lon, double lat, double alt, double re, double f, double[] rectan);

    // SpiceDouble phaseq_c (SpiceDouble et, ConstSpiceChar *target, ConstSpiceChar *illmn, ConstSpiceChar *obsrvr, ConstSpiceChar *abcorr);
    double phaseq_c(double et, String target, String illmn, String obsrvr, String abcorr);

    // SpiceDouble pi_c (void);
    double pi_c();

    // void pltexp_c (ConstSpiceDouble iverts[3][3], SpiceDouble delta, SpiceDouble overts[3][3]);
    void pltexp_c(double[] iverts, double delta, double[] overts);

    // void pltnp_c (ConstSpiceDouble point[3], ConstSpiceDouble v1[3], ConstSpiceDouble v2[3], ConstSpiceDouble v3[3], SpiceDouble pnear[3], SpiceDouble *dist);
    void pltnp_c(double[] point, double[] v1, double[] v2, double[] v3, double[] pnear, double[] dist);

    // void pltnrm_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3], ConstSpiceDouble v3[3], SpiceDouble normal[3]);
    void pltnrm_c(double[] v1, double[] v2, double[] v3, double[] normal);

    // void polyds_c (ConstSpiceDouble *coeffs, SpiceInt deg, SpiceInt nderiv, SpiceDouble t, SpiceDouble *p);
    void polyds_c(double[] coeffs, int deg, int nderiv, double t, double[] p);

    // SpiceInt pos_c (ConstSpiceChar *str, ConstSpiceChar *substr, SpiceInt start);
    int pos_c(String str, String substr, int start);

    // SpiceInt posr_c (ConstSpiceChar *str, ConstSpiceChar *substr, SpiceInt start);
    int posr_c(String str, String substr, int start);

    // SpiceChar * prompt_c (ConstSpiceChar *dspmsg, SpiceInt buflen, SpiceChar *buffer);
    String prompt_c(String dspmsg, int buflen, String buffer);

    // void prop2b_c (SpiceDouble gm, ConstSpiceDouble pvinit[6], SpiceDouble dt, SpiceDouble pvprop[6]);
    void prop2b_c(double gm, double[] pvinit, double dt, double[] pvprop);

    // void prsdp_c (ConstSpiceChar *string, SpiceDouble *dpval);
    void prsdp_c(String string, double[] dpval);

    // void prsint_c (ConstSpiceChar *string, SpiceInt *intval);
    void prsint_c(String string, int[] intval);

    // void pxform_c (ConstSpiceChar *from, ConstSpiceChar *to, SpiceDouble et, SpiceDouble rotate[3][3]);
    void pxform_c(String from, String to, double et, double[] rotate);

    // void pxfrm2_c (ConstSpiceChar *from, ConstSpiceChar *to, SpiceDouble etfrom, SpiceDouble etto, SpiceDouble rotate[3][3]);
    void pxfrm2_c(String from, String to, double etfrom, double etto, double[] rotate);

    // void q2m_c (ConstSpiceDouble q[4], SpiceDouble r[3][3]);
    void q2m_c(double[] q, double[] r);

    // void qcktrc_c (SpiceInt tracelen, SpiceChar *trace);
    void qcktrc_c(int tracelen, String trace);

    // void qderiv_c (SpiceInt ndim, ConstSpiceDouble f0[], ConstSpiceDouble f2[], SpiceDouble delta, SpiceDouble dfdt[]);
    void qderiv_c(int ndim, double[] f0, double[] f2, double delta, double[] dfdt);

    // void qdq2av_c (ConstSpiceDouble q[4], ConstSpiceDouble dq[4], SpiceDouble av[3]);
    void qdq2av_c(double[] q, double[] dq, double[] av);

    // void qxq_c (ConstSpiceDouble q1[4], ConstSpiceDouble q2[4], SpiceDouble qout[4]);
    void qxq_c(double[] q1, double[] q2, double[] qout);

    // void radrec_c (SpiceDouble range, SpiceDouble ra, SpiceDouble dec, SpiceDouble rectan[3]);
    void radrec_c(double range, double ra, double dec, double[] rectan);

    // void rav2xf_c (ConstSpiceDouble rot[3][3], ConstSpiceDouble av[3], SpiceDouble xform[6][6]);
    void rav2xf_c(double[] rot, double[] av, double[] xform);

    // void raxisa_c (ConstSpiceDouble matrix[3][3], SpiceDouble axis[3], SpiceDouble *angle);
    void raxisa_c(double[] matrix, double[] axis, double[] angle);

    // void rdtext_c (ConstSpiceChar *file, SpiceInt lineln, SpiceChar *line, SpiceBoolean *eof);
    void rdtext_c(String file, int lineln, String line, boolean[] eof);

    // void recazl_c (ConstSpiceDouble rectan[3], SpiceBoolean azccw, SpiceBoolean elplsz, SpiceDouble *range, SpiceDouble *az, SpiceDouble *el);
    void recazl_c(double[] rectan, boolean azccw, boolean elplsz, double[] range, double[] az, double[] el);

    // void reccyl_c (ConstSpiceDouble rectan[3], SpiceDouble *r, SpiceDouble *clon, SpiceDouble *z);
    void reccyl_c(double[] rectan, double[] r, double[] clon, double[] z);

    // void recgeo_c (ConstSpiceDouble rectan[3], SpiceDouble re, SpiceDouble f, SpiceDouble *lon, SpiceDouble *lat, SpiceDouble *alt);
    void recgeo_c(double[] rectan, double re, double f, double[] lon, double[] lat, double[] alt);

    // void reclat_c (ConstSpiceDouble rectan[3], SpiceDouble *radius, SpiceDouble *lon, SpiceDouble *lat);
    void reclat_c(double[] rectan, double[] radius, double[] lon, double[] lat);

    // void recpgr_c (ConstSpiceChar *body, SpiceDouble rectan[3], SpiceDouble re, SpiceDouble f, SpiceDouble *lon, SpiceDouble *lat, SpiceDouble *alt);
    void recpgr_c(String body, double[] rectan, double re, double f, double[] lon, double[] lat, double[] alt);

    // void recrad_c (ConstSpiceDouble rectan[3], SpiceDouble *range, SpiceDouble *ra, SpiceDouble *dec);
    void recrad_c(double[] rectan, double[] range, double[] ra, double[] dec);

    // void recsph_c (ConstSpiceDouble rectan[3], SpiceDouble *r, SpiceDouble *colat, SpiceDouble *slon);
    void recsph_c(double[] rectan, double[] r, double[] colat, double[] slon);

    // void repmc_c (ConstSpiceChar *in, ConstSpiceChar *marker, ConstSpiceChar *value, SpiceInt outlen, SpiceChar *out);
    void repmc_c(String in, String marker, String value, int outlen, String out);

    // void repmct_c (ConstSpiceChar *in, ConstSpiceChar *marker, SpiceInt value, SpiceChar rtcase, SpiceInt outlen, SpiceChar *out);
    void repmct_c(String in, String marker, int value, byte rtcase, int outlen, String out);

    // void repmd_c (ConstSpiceChar *in, ConstSpiceChar *marker, SpiceDouble value, SpiceInt sigdig, SpiceInt outlen, SpiceChar *out);
    void repmd_c(String in, String marker, double value, int sigdig, int outlen, String out);

    // void repmf_c (ConstSpiceChar *in, ConstSpiceChar *marker, SpiceDouble value, SpiceInt sigdig, SpiceChar format, SpiceInt outlen, SpiceChar *out);
    void repmf_c(String in, String marker, double value, int sigdig, byte format, int outlen, String out);

    // void repmi_c (ConstSpiceChar *in, ConstSpiceChar *marker, SpiceInt value, SpiceInt outlen, SpiceChar *out);
    void repmi_c(String in, String marker, int value, int outlen, String out);

    // void repml_c (ConstSpiceChar *in, ConstSpiceChar *marker, SpiceBoolean value, SpiceChar rtcase, SpiceInt outlen, SpiceChar *out);
    void repml_c(String in, String marker, boolean value, byte rtcase, int outlen, String out);

    // void repmot_c (ConstSpiceChar *in, ConstSpiceChar *marker, SpiceInt value, SpiceChar rtcase, SpiceInt outlen, SpiceChar *out);
    void repmot_c(String in, String marker, int value, byte rtcase, int outlen, String out);

    // void reset_c (void);
    void reset_c();

    // SpiceBoolean return_c (void);
    boolean return_c();

    // void rotate_c (SpiceDouble angle, SpiceInt iaxis, SpiceDouble mout[3][3]);
    void rotate_c(double angle, int iaxis, double[] mout);

    // void rotmat_c (ConstSpiceDouble m1[3][3], SpiceDouble angle, SpiceInt iaxis, SpiceDouble mout[3][3]);
    void rotmat_c(double[] m1, double angle, int iaxis, double[] mout);

    // void rotvec_c (ConstSpiceDouble v1[3], SpiceDouble angle, SpiceInt iaxis, SpiceDouble vout[3]);
    void rotvec_c(double[] v1, double angle, int iaxis, double[] vout);

    // SpiceDouble rpd_c (void);
    double rpd_c();

    // void rquad_c (SpiceDouble a, SpiceDouble b, SpiceDouble c, SpiceDouble root1[2], SpiceDouble root2[2]);
    void rquad_c(double a, double b, double c, double[] root1, double[] root2);

    // void saelgv_c (ConstSpiceDouble vec1[3], ConstSpiceDouble vec2[3], SpiceDouble smajor[3], SpiceDouble sminor[3]);
    void saelgv_c(double[] vec1, double[] vec2, double[] smajor, double[] sminor);

    // void scdecd_c (SpiceInt sc, SpiceDouble sclkdp, SpiceInt scllen, SpiceChar *sclkch);
    void scdecd_c(int sc, double sclkdp, int scllen, String sclkch);

    // void sce2c_c (SpiceInt sc, SpiceDouble et, SpiceDouble *sclkdp);
    void sce2c_c(int sc, double et, double[] sclkdp);

    // void sce2s_c (SpiceInt sc, SpiceDouble et, SpiceInt scllen, SpiceChar *sclkch);
    void sce2s_c(int sc, double et, int scllen, String sclkch);

    // void sce2t_c (SpiceInt sc, SpiceDouble et, SpiceDouble *sclkdp);
    void sce2t_c(int sc, double et, double[] sclkdp);

    // void scencd_c (SpiceInt sc, ConstSpiceChar *sclkch, SpiceDouble *sclkdp);
    void scencd_c(int sc, String sclkch, double[] sclkdp);

    // void scfmt_c (SpiceInt sc, SpiceDouble ticks, SpiceInt clklen, SpiceChar *clkstr);
    void scfmt_c(int sc, double ticks, int clklen, String clkstr);

    // void scpart_c (SpiceInt sc, SpiceInt *nparts, SpiceDouble pstart[], SpiceDouble pstop[]);
    void scpart_c(int sc, int[] nparts, double[] pstart, double[] pstop);

    // void scs2e_c (SpiceInt sc, ConstSpiceChar *sclkch, SpiceDouble *et);
    void scs2e_c(int sc, String sclkch, double[] et);

    // void sct2e_c (SpiceInt sc, SpiceDouble sclkdp, SpiceDouble *et);
    void sct2e_c(int sc, double sclkdp, double[] et);

    // void sctiks_c (SpiceInt sc, ConstSpiceChar *clkstr, SpiceDouble *ticks);
    void sctiks_c(int sc, String clkstr, double[] ticks);

    // void setmsg_c (ConstSpiceChar *msg);
    void setmsg_c(String msg);

    // void shellc_c (SpiceInt ndim, SpiceInt arrlen, void *array);
    void shellc_c(int ndim, int arrlen, byte[] array);

    // void shelld_c (SpiceInt ndim, SpiceDouble *array);
    void shelld_c(int ndim, double[] array);

    // void shelli_c (SpiceInt ndim, SpiceInt *array);
    void shelli_c(int ndim, int[] array);

    // void sigerr_c (ConstSpiceChar *msg);
    void sigerr_c(String msg);

    // void sincpt_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, ConstSpiceChar *dref, ConstSpiceDouble dvec[3], SpiceDouble spoint[3], SpiceDouble *trgepc, SpiceDouble srfvec[3], SpiceBoolean *found);
    void sincpt_c(String method, String target, double et, String fixref, String abcorr, String obsrvr, String dref, double[] dvec, double[] spoint, double[] trgepc, double[] srfvec, boolean[] found);

    // SpiceDouble spd_c (void);
    double spd_c();

    // void sphcyl_c (SpiceDouble radius, SpiceDouble colat, SpiceDouble slon, SpiceDouble *r, SpiceDouble *clon, SpiceDouble *z);
    void sphcyl_c(double radius, double colat, double slon, double[] r, double[] clon, double[] z);

    // void sphlat_c (SpiceDouble r, SpiceDouble colat, SpiceDouble slon, SpiceDouble *radius, SpiceDouble *lon, SpiceDouble *lat);
    void sphlat_c(double r, double colat, double slon, double[] radius, double[] lon, double[] lat);

    // void sphrec_c (SpiceDouble r, SpiceDouble colat, SpiceDouble slon, SpiceDouble rectan[3]);
    void sphrec_c(double r, double colat, double slon, double[] rectan);

    // void spk14a_c (SpiceInt handle, SpiceInt ncsets, ConstSpiceDouble coeffs[], ConstSpiceDouble epochs[]);
    void spk14a_c(int handle, int ncsets, double[] coeffs, double[] epochs);

    // void spk14b_c (SpiceInt handle, ConstSpiceChar *segid, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, SpiceInt chbdeg);
    void spk14b_c(int handle, String segid, int body, int center, String frame, double first, double last, int chbdeg);

    // void spk14e_c (SpiceInt handle);
    void spk14e_c(int handle);

    // void spkacs_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceChar *abcorr, SpiceInt obs, SpiceDouble starg[6], SpiceDouble *lt, SpiceDouble *dlt);
    void spkacs_c(int targ, double et, String ref, String abcorr, int obs, double[] starg, double[] lt, double[] dlt);

    // void spkapo_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceDouble sobs[6], ConstSpiceChar *abcorr, SpiceDouble ptarg[3], SpiceDouble *lt);
    void spkapo_c(int targ, double et, String ref, double[] sobs, String abcorr, double[] ptarg, double[] lt);

    // void spkapp_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceDouble sobs[6], ConstSpiceChar *abcorr, SpiceDouble starg[6], SpiceDouble *lt);
    void spkapp_c(int targ, double et, String ref, double[] sobs, String abcorr, double[] starg, double[] lt);

    // void spkaps_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceChar *abcorr, ConstSpiceDouble stobs[6], ConstSpiceDouble accobs[3], SpiceDouble starg[6], SpiceDouble *lt, SpiceDouble *dlt);
    void spkaps_c(int targ, double et, String ref, String abcorr, double[] stobs, double[] accobs, double[] starg, double[] lt, double[] dlt);

    // void spkcls_c (SpiceInt handle);
    void spkcls_c(int handle);

    // void spkcpo_c (ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *outref, ConstSpiceChar *refloc, ConstSpiceChar *abcorr, ConstSpiceDouble obspos[3], ConstSpiceChar *obsctr, ConstSpiceChar *obsref, SpiceDouble state[6], SpiceDouble *lt);
    void spkcpo_c(String target, double et, String outref, String refloc, String abcorr, double[] obspos, String obsctr, String obsref, double[] state, double[] lt);

    // void spkcpt_c (ConstSpiceDouble trgpos[3], ConstSpiceChar *trgctr, ConstSpiceChar *trgref, SpiceDouble et, ConstSpiceChar *outref, ConstSpiceChar *refloc, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble state[6], SpiceDouble *lt);
    void spkcpt_c(double[] trgpos, String trgctr, String trgref, double et, String outref, String refloc, String abcorr, String obsrvr, double[] state, double[] lt);

    // void spkcvo_c (ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *outref, ConstSpiceChar *refloc, ConstSpiceChar *abcorr, ConstSpiceDouble obssta[6], SpiceDouble obsepc, ConstSpiceChar *obsctr, ConstSpiceChar *obsref, SpiceDouble state[6], SpiceDouble *lt);
    void spkcvo_c(String target, double et, String outref, String refloc, String abcorr, double[] obssta, double obsepc, String obsctr, String obsref, double[] state, double[] lt);

    // void spkcvt_c (ConstSpiceDouble trgsta[6], SpiceDouble trgepc, ConstSpiceChar *trgctr, ConstSpiceChar *trgref, SpiceDouble et, ConstSpiceChar *outref, ConstSpiceChar *refloc, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble state[6], SpiceDouble *lt);
    void spkcvt_c(double[] trgsta, double trgepc, String trgctr, String trgref, double et, String outref, String refloc, String abcorr, String obsrvr, double[] state, double[] lt);

    // void spkez_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceChar *abcorr, SpiceInt obs, SpiceDouble starg[6], SpiceDouble *lt);
    void spkez_c(int targ, double et, String ref, String abcorr, int obs, double[] starg, double[] lt);

    // void spkezp_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceChar *abcorr, SpiceInt obs, SpiceDouble ptarg[3], SpiceDouble *lt);
    void spkezp_c(int targ, double et, String ref, String abcorr, int obs, double[] ptarg, double[] lt);

    // void spkezr_c (ConstSpiceChar *targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceChar *abcorr, ConstSpiceChar *obs, SpiceDouble starg[6], SpiceDouble *lt);
    void spkezr_c(String targ, double et, String ref, String abcorr, String obs, double[] starg, double[] lt);

    // void spkgeo_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, SpiceInt obs, SpiceDouble state[6], SpiceDouble *lt);
    void spkgeo_c(int targ, double et, String ref, int obs, double[] state, double[] lt);

    // void spkgps_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, SpiceInt obs, SpiceDouble pos[3], SpiceDouble *lt);
    void spkgps_c(int targ, double et, String ref, int obs, double[] pos, double[] lt);

    // void spklef_c (ConstSpiceChar *fname, SpiceInt *handle);
    void spklef_c(String fname, int[] handle);

    // void spkltc_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceChar *abcorr, ConstSpiceDouble stobs[6], SpiceDouble starg[6], SpiceDouble *lt, SpiceDouble *dlt);
    void spkltc_c(int targ, double et, String ref, String abcorr, double[] stobs, double[] starg, double[] lt, double[] dlt);

    // void spkopa_c (ConstSpiceChar *file, SpiceInt *handle);
    void spkopa_c(String file, int[] handle);

    // void spkopn_c (ConstSpiceChar *fname, ConstSpiceChar *ifname, SpiceInt ncomch, SpiceInt *handle);
    void spkopn_c(String fname, String ifname, int ncomch, int[] handle);

    // void spkpds_c (SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceInt type, SpiceDouble first, SpiceDouble last, SpiceDouble descr[5]);
    void spkpds_c(int body, int center, String frame, int type, double first, double last, double[] descr);

    // void spkpos_c (ConstSpiceChar *targ, SpiceDouble et, ConstSpiceChar *ref, ConstSpiceChar *abcorr, ConstSpiceChar *obs, SpiceDouble ptarg[3], SpiceDouble *lt);
    void spkpos_c(String targ, double et, String ref, String abcorr, String obs, double[] ptarg, double[] lt);

    // void spkpvn_c (SpiceInt handle, ConstSpiceDouble descr[5], SpiceDouble et, SpiceInt *ref, SpiceDouble state[6], SpiceInt *center);
    void spkpvn_c(int handle, double[] descr, double et, int[] ref, double[] state, int[] center);

    // void spksfs_c (SpiceInt body, SpiceDouble et, SpiceInt idlen, SpiceInt *handle, SpiceDouble descr[5], SpiceChar *ident, SpiceBoolean *found);
    void spksfs_c(int body, double et, int idlen, int[] handle, double[] descr, String ident, boolean[] found);

    // void spkssb_c (SpiceInt targ, SpiceDouble et, ConstSpiceChar *ref, SpiceDouble starg[6]);
    void spkssb_c(int targ, double et, String ref, double[] starg);

    // void spksub_c (SpiceInt handle, SpiceDouble descr[5], ConstSpiceChar *ident, SpiceDouble begin, SpiceDouble end, SpiceInt newh);
    void spksub_c(int handle, double[] descr, String ident, double begin, double end, int newh);

    // void spkuds_c (ConstSpiceDouble descr[5], SpiceInt *body, SpiceInt *center, SpiceInt *frame, SpiceInt *type, SpiceDouble *first, SpiceDouble *last, SpiceInt *baddrs, SpiceInt *eaddrs);
    void spkuds_c(double[] descr, int[] body, int[] center, int[] frame, int[] type, double[] first, double[] last, int[] baddrs, int[] eaddrs);

    // void spkuef_c (SpiceInt handle);
    void spkuef_c(int handle);

    // void spkw02_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceDouble intlen, SpiceInt n, SpiceInt polydg, ConstSpiceDouble cdata[], SpiceDouble btime);
    void spkw02_c(int handle, int body, int center, String frame, double first, double last, String segid, double intlen, int n, int polydg, double[] cdata, double btime);

    // void spkw03_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceDouble intlen, SpiceInt n, SpiceInt polydg, ConstSpiceDouble cdata[], SpiceDouble btime);
    void spkw03_c(int handle, int body, int center, String frame, double first, double last, String segid, double intlen, int n, int polydg, double[] cdata, double btime);

    // void spkw05_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceDouble gm, SpiceInt n, ConstSpiceDouble states[][6], ConstSpiceDouble epochs[]);
    void spkw05_c(int handle, int body, int center, String frame, double first, double last, String segid, double gm, int n, double[] states, double[] epochs);

    // void spkw08_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceInt degree, SpiceInt n, ConstSpiceDouble states[][6], SpiceDouble begtim, SpiceDouble step);
    void spkw08_c(int handle, int body, int center, String frame, double first, double last, String segid, int degree, int n, double[] states, double begtim, double step);

    // void spkw09_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceInt degree, SpiceInt n, ConstSpiceDouble states[][6], ConstSpiceDouble epochs[]);
    void spkw09_c(int handle, int body, int center, String frame, double first, double last, String segid, int degree, int n, double[] states, double[] epochs);

    // void spkw10_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, ConstSpiceDouble consts[8], SpiceInt n, ConstSpiceDouble elems[], ConstSpiceDouble epochs[]);
    void spkw10_c(int handle, int body, int center, String frame, double first, double last, String segid, double[] consts, int n, double[] elems, double[] epochs);

    // void spkw12_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceInt degree, SpiceInt n, ConstSpiceDouble states[][6], SpiceDouble begtim, SpiceDouble step);
    void spkw12_c(int handle, int body, int center, String frame, double first, double last, String segid, int degree, int n, double[] states, double begtim, double step);

    // void spkw13_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceInt degree, SpiceInt n, ConstSpiceDouble states[][6], ConstSpiceDouble epochs[]);
    void spkw13_c(int handle, int body, int center, String frame, double first, double last, String segid, int degree, int n, double[] states, double[] epochs);

    // void spkw15_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceDouble epoch, ConstSpiceDouble tp[3], ConstSpiceDouble pa[3], SpiceDouble p, SpiceDouble ecc, SpiceDouble j2flg, ConstSpiceDouble pv[3], SpiceDouble gm, SpiceDouble j2, SpiceDouble radius);
    void spkw15_c(int handle, int body, int center, String frame, double first, double last, String segid, double epoch, double[] tp, double[] pa, double p, double ecc, double j2flg, double[] pv, double gm, double j2, double radius);

    // void spkw17_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceDouble epoch, ConstSpiceDouble eqel[9], SpiceDouble rapol, SpiceDouble decpol);
    void spkw17_c(int handle, int body, int center, String frame, double first, double last, String segid, double epoch, double[] eqel, double rapol, double decpol);

    // void spkw20_c (SpiceInt handle, SpiceInt body, SpiceInt center, ConstSpiceChar *frame, SpiceDouble first, SpiceDouble last, ConstSpiceChar *segid, SpiceDouble intlen, SpiceInt n, SpiceInt polydg, ConstSpiceDouble cdata[], SpiceDouble dscale, SpiceDouble tscale, SpiceDouble initjd, SpiceDouble initfr);
    void spkw20_c(int handle, int body, int center, String frame, double first, double last, String segid, double intlen, int n, int polydg, double[] cdata, double dscale, double tscale, double initjd, double initfr);

    // void srfc2s_c (SpiceInt code, SpiceInt bodyid, SpiceInt srflen, SpiceChar *srfstr, SpiceBoolean *isname);
    void srfc2s_c(int code, int bodyid, int srflen, String srfstr, boolean[] isname);

    // void srfcss_c (SpiceInt code, ConstSpiceChar *bodstr, SpiceInt srflen, SpiceChar *srfstr, SpiceBoolean *isname);
    void srfcss_c(int code, String bodstr, int srflen, String srfstr, boolean[] isname);

    // void srfnrm_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, SpiceInt npts, ConstSpiceDouble srfpts[][3], SpiceDouble normls[][3]);
    void srfnrm_c(String method, String target, double et, String fixref, int npts, double[] srfpts, double[] normls);

    // void srfrec_c (SpiceInt body, SpiceDouble lon, SpiceDouble lat, SpiceDouble rectan[3]);
    void srfrec_c(int body, double lon, double lat, double[] rectan);

    // void srfs2c_c (ConstSpiceChar *srfstr, ConstSpiceChar *bodstr, SpiceInt *code, SpiceBoolean *found);
    void srfs2c_c(String srfstr, String bodstr, int[] code, boolean[] found);

    // void srfscc_c (ConstSpiceChar *srfstr, SpiceInt bodyid, SpiceInt *code, SpiceBoolean *found);
    void srfscc_c(String srfstr, int bodyid, int[] code, boolean[] found);

    // void srfxpt_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, ConstSpiceChar *dref, ConstSpiceDouble dvec[3], SpiceDouble spoint[3], SpiceDouble *dist, SpiceDouble *trgepc, SpiceDouble obspos[3], SpiceBoolean *found);
    void srfxpt_c(String method, String target, double et, String abcorr, String obsrvr, String dref, double[] dvec, double[] spoint, double[] dist, double[] trgepc, double[] obspos, boolean[] found);

    // void stelab_c (ConstSpiceDouble pobj[3], ConstSpiceDouble vobs[3], SpiceDouble appobj[3]);
    void stelab_c(double[] pobj, double[] vobs, double[] appobj);

    // void stlabx_c (ConstSpiceDouble pobj[3], ConstSpiceDouble vobs[3], SpiceDouble corpos[3]);
    void stlabx_c(double[] pobj, double[] vobs, double[] corpos);

    // void stpool_c (ConstSpiceChar *item, SpiceInt nth, ConstSpiceChar *contin, SpiceInt nthlen, SpiceChar *nthstr, SpiceInt *size, SpiceBoolean *found);
    void stpool_c(String item, int nth, String contin, int nthlen, String nthstr, int[] size, boolean[] found);

    // void str2et_c (ConstSpiceChar *timstr, SpiceDouble *et);
    void str2et_c(String timstr, double[] et);

    // void subpnt_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble spoint[3], SpiceDouble *trgepc, SpiceDouble srfvec[3]);
    void subpnt_c(String method, String target, double et, String fixref, String abcorr, String obsrvr, double[] spoint, double[] trgepc, double[] srfvec);

    // void subpt_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble spoint[3], SpiceDouble *alt);
    void subpt_c(String method, String target, double et, String abcorr, String obsrvr, double[] spoint, double[] alt);

    // void subslr_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble spoint[3], SpiceDouble *trgepc, SpiceDouble srfvec[3]);
    void subslr_c(String method, String target, double et, String fixref, String abcorr, String obsrvr, double[] spoint, double[] trgepc, double[] srfvec);

    // void subsol_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *abcorr, ConstSpiceChar *obsrvr, SpiceDouble spoint[3]);
    void subsol_c(String method, String target, double et, String abcorr, String obsrvr, double[] spoint);

    // SpiceDouble sumad_c (ConstSpiceDouble *array, SpiceInt n);
    double sumad_c(double[] array, int n);

    // void surfnm_c (SpiceDouble a, SpiceDouble b, SpiceDouble c, ConstSpiceDouble point[3], SpiceDouble normal[3]);
    void surfnm_c(double a, double b, double c, double[] point, double[] normal);

    // void surfpt_c (ConstSpiceDouble positn[3], ConstSpiceDouble u[3], SpiceDouble a, SpiceDouble b, SpiceDouble c, SpiceDouble point[3], SpiceBoolean *found);
    void surfpt_c(double[] positn, double[] u, double a, double b, double c, double[] point, boolean[] found);

    // void surfpv_c (ConstSpiceDouble stvrtx[6], ConstSpiceDouble stdir[6], SpiceDouble a, SpiceDouble b, SpiceDouble c, SpiceDouble stx[6], SpiceBoolean *found);
    void surfpv_c(double[] stvrtx, double[] stdir, double a, double b, double c, double[] stx, boolean[] found);

    // void sxform_c (ConstSpiceChar *from, ConstSpiceChar *to, SpiceDouble et, SpiceDouble xform[6][6]);
    void sxform_c(String from, String to, double et, double[] xform);

    // void szpool_c (ConstSpiceChar *name, SpiceInt *n, SpiceBoolean *found);
    void szpool_c(String name, int[] n, boolean[] found);

    // void tangpt_c (ConstSpiceChar *method, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *corloc, ConstSpiceChar *obsrvr, ConstSpiceChar *dref, ConstSpiceDouble dvec[3], SpiceDouble tanpt[3], SpiceDouble *alt, SpiceDouble *range, SpiceDouble srfpt[3], SpiceDouble *trgepc, SpiceDouble srfvec[3]);
    void tangpt_c(String method, String target, double et, String fixref, String abcorr, String corloc, String obsrvr, String dref, double[] dvec, double[] tanpt, double[] alt, double[] range, double[] srfpt, double[] trgepc, double[] srfvec);

    // void termpt_c (ConstSpiceChar *method, ConstSpiceChar *ilusrc, ConstSpiceChar *target, SpiceDouble et, ConstSpiceChar *fixref, ConstSpiceChar *abcorr, ConstSpiceChar *corloc, ConstSpiceChar *obsrvr, ConstSpiceDouble refvec[3], SpiceDouble rolstp, SpiceInt ncuts, SpiceDouble schstp, SpiceDouble soltol, SpiceInt maxn, SpiceInt npts[], SpiceDouble points[][3], SpiceDouble epochs[], SpiceDouble trmvcs[][3]);
    void termpt_c(String method, String ilusrc, String target, double et, String fixref, String abcorr, String corloc, String obsrvr, double[] refvec, double rolstp, int ncuts, double schstp, double soltol, int maxn, int[] npts, double[] points, double[] epochs, double[] trmvcs);

    // void timdef_c (ConstSpiceChar *action, ConstSpiceChar *item, SpiceInt vallen, SpiceChar *value);
    void timdef_c(String action, String item, int vallen, String value);

    // void timout_c (SpiceDouble et, ConstSpiceChar *pictur, SpiceInt outlen, SpiceChar *output);
    void timout_c(double et, String pictur, int outlen, String output);

    // void tipbod_c (ConstSpiceChar *ref, SpiceInt body, SpiceDouble et, SpiceDouble tipm[3][3]);
    void tipbod_c(String ref, int body, double et, double[] tipm);

    // void tisbod_c (ConstSpiceChar *ref, SpiceInt body, SpiceDouble et, SpiceDouble tsipm[6][6]);
    void tisbod_c(String ref, int body, double et, double[] tsipm);

    // void tkfram_c (SpiceInt frcode, SpiceDouble rot[3][3], SpiceInt *frame, SpiceBoolean *found);
    void tkfram_c(int frcode, double[] rot, int[] frame, boolean[] found);

    // ConstSpiceChar * tkvrsn_c (ConstSpiceChar *item);
    String tkvrsn_c(String item);

    // void tparch_c (ConstSpiceChar *type);
    void tparch_c(String type);

    // void tparse_c (ConstSpiceChar *string, SpiceInt errmln, SpiceDouble *sp2000, SpiceChar *errmsg);
    void tparse_c(String string, int errmln, double[] sp2000, String errmsg);

    // void tpictr_c (ConstSpiceChar *sample, SpiceInt pictln, SpiceInt errmln, SpiceChar *pictur, SpiceBoolean *ok, SpiceChar *errmsg);
    void tpictr_c(String sample, int pictln, int errmln, String pictur, boolean[] ok, String errmsg);

    // SpiceDouble trace_c (ConstSpiceDouble matrix[3][3]);
    double trace_c(double[] matrix);

    // void trcdep_c (SpiceInt *depth);
    void trcdep_c(int[] depth);

    // void trcnam_c (SpiceInt index, SpiceInt namlen, SpiceChar *name);
    void trcnam_c(int index, int namlen, String name);

    // void trcoff_c (void);
    void trcoff_c();

    // SpiceDouble trgsep_c (SpiceDouble et, ConstSpiceChar *targ1, ConstSpiceChar *shape1, ConstSpiceChar *frame1, ConstSpiceChar *targ2, ConstSpiceChar *shape2, ConstSpiceChar *frame2, ConstSpiceChar *obsrvr, ConstSpiceChar *abcorr);
    double trgsep_c(double et, String targ1, String shape1, String frame1, String targ2, String shape2, String frame2, String obsrvr, String abcorr);

    // void tsetyr_c (SpiceInt year);
    void tsetyr_c(int year);

    // SpiceDouble twopi_c (void);
    double twopi_c();

    // void twovec_c (ConstSpiceDouble axdef[3], SpiceInt indexa, ConstSpiceDouble plndef[3], SpiceInt indexp, SpiceDouble mout[3][3]);
    void twovec_c(double[] axdef, int indexa, double[] plndef, int indexp, double[] mout);

    // void twovxf_c (ConstSpiceDouble axdef[6], SpiceInt indexa, ConstSpiceDouble plndef[6], SpiceInt indexp, SpiceDouble xform[6][6]);
    void twovxf_c(double[] axdef, int indexa, double[] plndef, int indexp, double[] xform);

    // SpiceDouble tyear_c (void);
    double tyear_c();

    // void ucase_c (SpiceChar *in, SpiceInt outlen, SpiceChar *out);
    void ucase_c(String in, int outlen, String out);

    // void ucrss_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3], SpiceDouble vout[3]);
    void ucrss_c(double[] v1, double[] v2, double[] vout);

    // void udf_c (SpiceDouble x, SpiceDouble *value);
    void udf_c(double x, double[] value);

    // SpiceDouble unitim_c (SpiceDouble epoch, ConstSpiceChar *insys, ConstSpiceChar *outsys);
    double unitim_c(double epoch, String insys, String outsys);

    // void unload_c (ConstSpiceChar *file);
    void unload_c(String file);

    // void unorm_c (ConstSpiceDouble v1[3], SpiceDouble vout[3], SpiceDouble *vmag);
    void unorm_c(double[] v1, double[] vout, double[] vmag);

    // void unormg_c (ConstSpiceDouble v1[], SpiceInt ndim, SpiceDouble vout[], SpiceDouble *vmag);
    void unormg_c(double[] v1, int ndim, double[] vout, double[] vmag);

    // void utc2et_c (ConstSpiceChar *utcstr, SpiceDouble *et);
    void utc2et_c(String utcstr, double[] et);

    // void vadd_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3], SpiceDouble vout[3]);
    void vadd_c(double[] v1, double[] v2, double[] vout);

    // void vaddg_c (ConstSpiceDouble *v1, ConstSpiceDouble *v2, SpiceInt ndim, SpiceDouble *vout);
    void vaddg_c(double[] v1, double[] v2, int ndim, double[] vout);

    // void vcrss_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3], SpiceDouble vout[3]);
    void vcrss_c(double[] v1, double[] v2, double[] vout);

    // SpiceDouble vdist_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3]);
    double vdist_c(double[] v1, double[] v2);

    // SpiceDouble vdistg_c (ConstSpiceDouble *v1, ConstSpiceDouble *v2, SpiceInt ndim);
    double vdistg_c(double[] v1, double[] v2, int ndim);

    // SpiceDouble vdot_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3]);
    double vdot_c(double[] v1, double[] v2);

    // SpiceDouble vdotg_c (ConstSpiceDouble *v1, ConstSpiceDouble *v2, SpiceInt ndim);
    double vdotg_c(double[] v1, double[] v2, int ndim);

    // void vequ_c (ConstSpiceDouble vin[3], SpiceDouble vout[3]);
    void vequ_c(double[] vin, double[] vout);

    // void vequg_c (ConstSpiceDouble *vin, SpiceInt ndim, SpiceDouble *vout);
    void vequg_c(double[] vin, int ndim, double[] vout);

    // void vhat_c (ConstSpiceDouble v1[3], SpiceDouble vout[3]);
    void vhat_c(double[] v1, double[] vout);

    // void vhatg_c (ConstSpiceDouble *v1, SpiceInt ndim, SpiceDouble *vout);
    void vhatg_c(double[] v1, int ndim, double[] vout);

    // void vlcom3_c (SpiceDouble a, ConstSpiceDouble v1[3], SpiceDouble b, ConstSpiceDouble v2[3], SpiceDouble c, ConstSpiceDouble v3[3], SpiceDouble sum[3]);
    void vlcom3_c(double a, double[] v1, double b, double[] v2, double c, double[] v3, double[] sum);

    // void vlcom_c (SpiceDouble a, ConstSpiceDouble v1[3], SpiceDouble b, ConstSpiceDouble v2[3], SpiceDouble sum[3]);
    void vlcom_c(double a, double[] v1, double b, double[] v2, double[] sum);

    // void vlcomg_c (SpiceInt n, SpiceDouble a, ConstSpiceDouble *v1, SpiceDouble b, ConstSpiceDouble *v2, SpiceDouble *sum);
    void vlcomg_c(int n, double a, double[] v1, double b, double[] v2, double[] sum);

    // void vminug_c (ConstSpiceDouble *vin, SpiceInt ndim, SpiceDouble *vout);
    void vminug_c(double[] vin, int ndim, double[] vout);

    // void vminus_c (ConstSpiceDouble v1[3], SpiceDouble vout[3]);
    void vminus_c(double[] v1, double[] vout);

    // SpiceDouble vnorm_c (ConstSpiceDouble v1[3]);
    double vnorm_c(double[] v1);

    // SpiceDouble vnormg_c (ConstSpiceDouble *v1, SpiceInt ndim);
    double vnormg_c(double[] v1, int ndim);

    // void vpack_c (SpiceDouble x, SpiceDouble y, SpiceDouble z, SpiceDouble v[3]);
    void vpack_c(double x, double y, double z, double[] v);

    // void vperp_c (ConstSpiceDouble a[3], ConstSpiceDouble b[3], SpiceDouble p[3]);
    void vperp_c(double[] a, double[] b, double[] p);

    // void vproj_c (ConstSpiceDouble a[3], ConstSpiceDouble b[3], SpiceDouble p[3]);
    void vproj_c(double[] a, double[] b, double[] p);

    // void vprojg_c (ConstSpiceDouble a[], ConstSpiceDouble b[], SpiceInt ndim, SpiceDouble p[]);
    void vprojg_c(double[] a, double[] b, int ndim, double[] p);

    // SpiceDouble vrel_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3]);
    double vrel_c(double[] v1, double[] v2);

    // SpiceDouble vrelg_c (ConstSpiceDouble *v1, ConstSpiceDouble *v2, SpiceInt ndim);
    double vrelg_c(double[] v1, double[] v2, int ndim);

    // void vrotv_c (ConstSpiceDouble v[3], ConstSpiceDouble axis[3], SpiceDouble theta, SpiceDouble r[3]);
    void vrotv_c(double[] v, double[] axis, double theta, double[] r);

    // void vscl_c (SpiceDouble s, ConstSpiceDouble v1[3], SpiceDouble vout[3]);
    void vscl_c(double s, double[] v1, double[] vout);

    // void vsclg_c (SpiceDouble s, ConstSpiceDouble *v1, SpiceInt ndim, SpiceDouble *vout);
    void vsclg_c(double s, double[] v1, int ndim, double[] vout);

    // SpiceDouble vsep_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3]);
    double vsep_c(double[] v1, double[] v2);

    // SpiceDouble vsepg_c (ConstSpiceDouble *v1, ConstSpiceDouble *v2, SpiceInt ndim);
    double vsepg_c(double[] v1, double[] v2, int ndim);

    // void vsub_c (ConstSpiceDouble v1[3], ConstSpiceDouble v2[3], SpiceDouble vout[3]);
    void vsub_c(double[] v1, double[] v2, double[] vout);

    // void vsubg_c (ConstSpiceDouble *v1, ConstSpiceDouble *v2, SpiceInt ndim, SpiceDouble *vout);
    void vsubg_c(double[] v1, double[] v2, int ndim, double[] vout);

    // SpiceDouble vtmv_c (ConstSpiceDouble v1[3], ConstSpiceDouble matrix[3][3], ConstSpiceDouble v2[3]);
    double vtmv_c(double[] v1, double[] matrix, double[] v2);

    // void vupack_c (ConstSpiceDouble v[3], SpiceDouble *x, SpiceDouble *y, SpiceDouble *z);
    void vupack_c(double[] v, double[] x, double[] y, double[] z);

    // SpiceBoolean vzero_c (ConstSpiceDouble v[3]);
    boolean vzero_c(double[] v);

    // SpiceBoolean vzerog_c (ConstSpiceDouble *v, SpiceInt ndim);
    boolean vzerog_c(double[] v, int ndim);

    // void xf2eul_c (ConstSpiceDouble xform[6][6], SpiceInt axisa, SpiceInt axisb, SpiceInt axisc, SpiceDouble eulang[6], SpiceBoolean *unique);
    void xf2eul_c(double[] xform, int axisa, int axisb, int axisc, double[] eulang, boolean[] unique);

    // void xf2rav_c (ConstSpiceDouble xform[6][6], SpiceDouble rot[3][3], SpiceDouble av[3]);
    void xf2rav_c(double[] xform, double[] rot, double[] av);

    // void xfmsta_c (ConstSpiceDouble istate[6], ConstSpiceChar *icosys, ConstSpiceChar *ocosys, ConstSpiceChar *body, SpiceDouble ostate[6]);
    void xfmsta_c(double[] istate, String icosys, String ocosys, String body, double[] ostate);

    // void xpose6_c (ConstSpiceDouble m1[6][6], SpiceDouble mout[6][6]);
    void xpose6_c(double[] m1, double[] mout);

    // void xpose_c (ConstSpiceDouble m1[3][3], SpiceDouble mout[3][3]);
    void xpose_c(double[] m1, double[] mout);
}
