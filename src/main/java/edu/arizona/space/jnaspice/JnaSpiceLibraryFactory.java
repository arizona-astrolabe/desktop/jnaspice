/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package edu.arizona.space.jnaspice;

import com.sun.jna.Native;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A factory class to supply a caller with a JNA library instance.
 *
 * @author agardner
 */
public class JnaSpiceLibraryFactory {

    private static JnaSpiceLibrary jsl = null;

    static {
        String os = System.getProperty("os.name").toLowerCase();
        switch (os) {
            case "windows 11" -> {
                try {
                    String temp = "C:/temp";
                    new File(temp).mkdirs();
                    InputStream is = JnaSpiceLibraryFactory.class.getResourceAsStream("/edu/arizona/space/jnaspice/cspice.dll");
                    Path tempfile = Files.createTempFile(Paths.get(temp), "jnaspice-", ".dll");
                    Files.write(tempfile, is.readAllBytes());
                    System.setProperty("jna.library.path", temp);
                    String name = tempfile.getFileName().toString();
                    jsl = (JnaSpiceLibrary) Native.load(name, JnaSpiceLibrary.class);
                } catch (IOException ex) {
                    ex.printStackTrace(System.err);
                }
            }
            case "linux" -> {
                try {
                    String temp = "/tmp/";
                    InputStream is = JnaSpiceLibraryFactory.class.getResourceAsStream("/edu/arizona/space/jnaspice/libcspice.so");
                    Path tempfile = Files.createTempFile(Paths.get(temp), "libjnaspice-", ".so");
                    Files.write(tempfile, is.readAllBytes());
                    System.setProperty("jna.library.path", temp);
                    String name = tempfile.getFileName().toString();
                    jsl = (JnaSpiceLibrary) Native.load(name, JnaSpiceLibrary.class);
                } catch (IOException ex) {
                    ex.printStackTrace(System.err);
                }
            }
            case "mac os x" -> {
                try {
                    String temp = "/tmp/";
                    InputStream is = JnaSpiceLibraryFactory.class.getResourceAsStream("/edu/arizona/space/jnaspice/libcspice.dylib");
                    Path tempfile = Files.createTempFile(Paths.get(temp), "libjnaspice-", ".dylib");
                    Files.write(tempfile, is.readAllBytes());
                    System.setProperty("jna.library.path", temp);
                    String name = tempfile.getFileName().toString();
                    jsl = (JnaSpiceLibrary) Native.load(name, JnaSpiceLibrary.class);
                } catch (IOException ex) {
                    ex.printStackTrace(System.err);
                }
            }
            default -> {
                jsl = null;
            }
        }
    }

    public static JnaSpiceLibrary getInstance() {
        return jsl;
    }
}
